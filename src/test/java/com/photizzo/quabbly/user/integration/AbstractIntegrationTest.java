package com.photizzo.quabbly.user.integration;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.photizzo.quabbly.user.dto.enums.Status;
import com.photizzo.quabbly.user.dto.output.BasicResponseDTO;
import com.photizzo.quabbly.user.integration.helpers.LoginResponseDTO;
import com.photizzo.quabbly.user.integration.helpers.LoginUser;
import com.photizzo.quabbly.user.integration.helpers.RegisterUser;
import com.photizzo.quabbly.user.integration.helpers.RegisterUserResponseDTO;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AbstractIntegrationTest {

    static TestRestTemplate restTemplate = new TestRestTemplate();

    static HttpHeaders headers;

    static String testUserFirstname = null;

    static String testUserLastname = null;

    static String testUserEmail = null;

    static void createAndlogInUser(int localServerPort) {

        if (testUserEmail != null) {
            return;
        }

        String firstname = RandomStringUtils.randomAlphabetic(6).toLowerCase();
        String lastname = RandomStringUtils.randomAlphabetic(6).toLowerCase();
        String email = firstname + "." + lastname + "@quabbly.com";
        String password = RandomStringUtils.randomAlphanumeric(10);
        String companyName = "Quabbly Inc";

        testUserFirstname = firstname;
        testUserLastname = lastname;
        testUserEmail = email;

        RegisterUser registerUser = new RegisterUser(firstname, lastname, email, password, companyName);
        ResponseEntity<RegisterUserResponseDTO> registerResponseEntity =
                restTemplate.postForEntity("http://localhost:" + localServerPort + "/v1/auth/register", registerUser, RegisterUserResponseDTO.class);

        assertNotNull(registerResponseEntity);
        assertEquals(HttpStatus.OK, registerResponseEntity.getStatusCode());

        LoginUser u = new LoginUser();
        u.setUsername(email);
        u.setPassword(password);

        ResponseEntity<LoginResponseDTO> responseEntity =
                restTemplate.postForEntity("http://localhost:" + localServerPort + "/v1/auth/login", u, LoginResponseDTO.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        LoginResponseDTO responseDTO = responseEntity.getBody();

        assertNotNull(responseDTO);
        assertNotNull(responseDTO.getToken());
        assertNotNull(responseDTO.getUser());
        assertEquals(responseDTO.getUser().getEmail(), email);

        System.out.println("AUTHORIZATION " + responseDTO.getToken());
        headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + responseDTO.getToken());

    }


    protected void assertErrorSize(int size, String result) {
        JsonArray errors = new JsonParser().parse(result).getAsJsonObject().getAsJsonArray("data");
        assertEquals(size, errors.size());
    }

    protected void assertError(String field, String message, String rejectedValue, String result) {
        assertErrorSize(1, result);
        assertError(field, message, rejectedValue, result, 0);
    }

    protected void assertError(String field, String message, String rejectedValue, String result, int position) {
        BasicResponseDTO response = new Gson().fromJson(result, BasicResponseDTO.class);
        Assert.assertEquals(Status.FAILED_VALIDATION, response.getStatus());

        JsonArray errors = new JsonParser().parse(result).getAsJsonObject().getAsJsonArray("data");


        assertEquals(field, errors.get(position).getAsJsonObject().get("field").getAsString());
        assertEquals(message, errors.get(position).getAsJsonObject().get("message").getAsString());

        if (rejectedValue != null) {
            assertEquals(rejectedValue, errors.get(position).getAsJsonObject().get("rejectedValue").getAsString());
        }
    }
}
