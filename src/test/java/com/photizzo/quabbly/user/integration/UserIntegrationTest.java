package com.photizzo.quabbly.user.integration;


import com.google.gson.Gson;
import com.photizzo.quabbly.user.config.TokenProvider;
import com.photizzo.quabbly.user.dto.enums.Status;
import com.photizzo.quabbly.user.dto.input.RegisterUserInputDto;
import com.photizzo.quabbly.user.dto.output.AllUsersResponseDTO;
import com.photizzo.quabbly.user.dto.output.UserResponseDTO;
import com.photizzo.quabbly.user.model.User;
import com.photizzo.quabbly.user.repository.UserRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserIntegrationTest extends AbstractIntegrationTest {


    @LocalServerPort
    private int localServerPort;


    @Mock
    private TokenProvider tokenProvider;

    @Autowired
    private UserRepository userRepository;

    @BeforeClass
    public static void configuration() {
        System.setProperty("mongodb.host", "localhost");
        System.setProperty("mongodb.port", "27017");
        System.setProperty("database.name", "userapi_test");
    }

    private static boolean started = false;

    @Before
    public void setup() {
        if(!started) {
            userRepository.deleteAll();
            createAndlogInUser(localServerPort);
            started = true;
        }
    }

    private User createUser() {
        String firstname = RandomStringUtils.randomAlphabetic(6).toLowerCase();
        String lastname = RandomStringUtils.randomAlphabetic(6).toLowerCase();
        String email = firstname + "." + lastname + "@quabbly.com";
        String password = RandomStringUtils.randomAlphanumeric(10);

        RegisterUserInputDto dto = new RegisterUserInputDto(firstname, lastname, email, password);

        HttpEntity entity = new HttpEntity<>(dto, headers);

        User u = userRepository.findByEmail(testUserEmail);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity("http://localhost:" + localServerPort + "/v1/users/add", entity, String.class);

        assertNotNull(responseEntity);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        UserResponseDTO responseDTO = new Gson().fromJson(responseEntity.getBody(), UserResponseDTO.class);
        assertEquals(Status.SUCCESS, responseDTO.getStatus());
        assertNotNull(responseDTO.getUser());
        assertNotNull(responseDTO.getUser().getId());
        assertNotNull(responseDTO.getUser().getCreatedDate());
        assertEquals(email, responseDTO.getUser().getEmail());
        assertEquals(firstname, responseDTO.getUser().getFirstname());
        assertEquals(lastname, responseDTO.getUser().getLastname());

        return responseDTO.getUser();
    }

    @Test
    public void when_add_user_with_valid_data_returns_successful() {
        createUser();
    }

    //TODO: tests for failed scenarios

    @Test
    public void when_fetch_all_users_returns_successful() {

        User u = userRepository.findByEmail(testUserEmail);

        HttpEntity entity = new HttpEntity<>(headers);

        ResponseEntity<String> responseEntity =
                restTemplate.exchange("http://localhost:" + localServerPort + "/v1/users/all", HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        AllUsersResponseDTO users = new Gson().fromJson(responseEntity.getBody(), AllUsersResponseDTO.class);

        assertEquals(Status.SUCCESS, users.getStatus());
        assertNotNull(users.getData());
        assertNotNull(users.getData().get(0));

        User firstActualUser = users.getData().get(0);

        assertEquals(testUserFirstname, firstActualUser.getFirstname());
        assertEquals(testUserLastname, firstActualUser.getLastname());
        assertEquals(testUserEmail, firstActualUser.getEmail());
        assertNotNull(firstActualUser.getCreatedDate());
    }


    @After
    public void cleanUp() {

    }


}
