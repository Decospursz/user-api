package com.photizzo.quabbly.user.integration.helpers;


import com.photizzo.quabbly.user.dto.enums.Status;
import com.photizzo.quabbly.user.dto.output.StandardResponseDTO;

public class RegisterUserResponseDTO extends StandardResponseDTO {

    private RegisterUser user;

    public RegisterUserResponseDTO() {
    }

    public RegisterUserResponseDTO(Status status) {
        super(status);
    }

    public RegisterUserResponseDTO(Status status, RegisterUser user) {
        super(status);
        this.user = user;
    }

    public RegisterUser getUser() {
        return user;
    }

    public void setUser(RegisterUser user) {
        this.user = user;
    }
}
