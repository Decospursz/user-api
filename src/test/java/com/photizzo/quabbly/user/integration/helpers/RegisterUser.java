package com.photizzo.quabbly.user.integration.helpers;

public class RegisterUser {

    private String firstname;
    private String lastname;
    private String email;
    private String password;
    private String companyName;


    public RegisterUser() {
    }

    public RegisterUser(String firstname, String lastname, String email, String password, String companyName) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
        this.companyName = companyName;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getCompanyName() {
        return companyName;
    }
}
