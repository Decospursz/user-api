package com.photizzo.quabbly.user.mailbox;


import java.util.ArrayList;
import java.util.List;

public class MessagePart {

    private String htmlContent;
    private String textContent;
    private String contentType;

    private List<MessagePartAttachment> attachments;
    private List<String> images;

    MessagePart() {
        this.htmlContent = "";
        this.textContent = "";
        this.attachments = new ArrayList<>();
        this.images = new ArrayList<>();
    }

    MessagePart(String textContent) {
        this();
        this.textContent = textContent;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }


    public List<String> getImages() {
        return images;
    }

    public void addImage(String image) {
        this.images.add(image);
    }


    String getHtmlContent() {
        return htmlContent;
    }

    void setHtmlContent(String htmlContent) {
        this.htmlContent = htmlContent;
    }

    String getTextContent() {
        return textContent;
    }

    void setTextContent(String textContent) {
        this.textContent = textContent;
    }

    List<MessagePartAttachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<MessagePartAttachment> attachments) {
        this.attachments = attachments;
    }
}
