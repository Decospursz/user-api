package com.photizzo.quabbly.user.mailbox;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.mail.*;
import java.util.*;

@Component
public class MailSessionHolder {

    private static Map<String, Session> smtpSessionStore = new HashMap<>();
    private static Map<String, Folder> inboxFolder = new HashMap<>();
    private static Map<String, Folder> newslettersFolder = new HashMap<>();
    private static Map<String, Folder> alertsFolder = new HashMap<>();
    private static Map<String, Folder> sentFolder = new HashMap<>();
    private static Map<String, Folder> spamFolder = new HashMap<>();
    private static Map<String, Folder> trashFolder = new HashMap<>();
    private static Map<String, Date> timer = new HashMap<>();
    private static String QUABBLY_HOST = "mx1.quabbly.com";

    public static Folder getFromSentFolder(String userId) {
        updateTime(userId);
        return sentFolder.get(userId);
    }

    private static void addToSMTPStore(String userId, Session session) {
        smtpSessionStore.put(userId, session);
    }

    public static Folder getFromInboxFolder(String userId) {
        updateTime(userId);
        return inboxFolder.get(userId);
    }

    public static Folder getFromNewslettersFolder(String userId) {
        updateTime(userId);
        return newslettersFolder.get(userId);
    }

    public static Folder getFromAlertsFolder(String userId) {
        updateTime(userId);
        return alertsFolder.get(userId);
    }

    public static Folder getFromSpamFolder(String userId) {
        updateTime(userId);
        return spamFolder.get(userId);
    }

    public static Folder getFromTrashFolder(String userId) {
        updateTime(userId);
        return trashFolder.get(userId);
    }

    public static void updateTime(String userId){
        timer.replace(userId, new Date());
    }

    public static void login(String userId, final String username, final String password) {
        try {

            if(userAlreadyExistsInMailStore(userId)){
                return;
            }

            Store store = getStore(username, password, QUABBLY_HOST);

            addToInboxFolder(userId, store);
            addNewslettersFolder(userId, store);
            addToAlertsFolder(userId, store);
            addToSentFolder(userId, store);
            addToSpamFolder(userId, store);
            addToTrashFolder(userId, store);


            System.out.println("user was logged in to mailbox: " + userId);

            Properties smtpProperties = new Properties();
            smtpProperties.put("mail.smtp.host", QUABBLY_HOST); //SMTP Host
            smtpProperties.put("mail.smtp.port", "587"); //TLS Port
            smtpProperties.put("mail.smtp.auth", "true"); //enable authentication
            smtpProperties.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS

            //create Authenticator object to pass in Session.getInstance argument
            Authenticator auth = new Authenticator() {
                //override the getPasswordAuthentication method
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            };

            addToSMTPStore(userId, Session.getInstance(smtpProperties, auth));
            timer.put(userId, new Date());

            System.out.println("user logged in to mailbox: " );

        } catch (Exception ex) {
            System.out.println("user wasnt logged in to mailbox: " );
        }
    }

    public static Store getDotComStore(String username, String password) throws MessagingException {
        String protocol = "imap";
        String port = "993";


        Properties properties = getServerProperties(protocol, "mail.quabbly.com", port);
        Session session = Session.getDefaultInstance(properties);

        Store store = session.getStore(protocol);
        store.connect("mail.quabbly.com", 993, username, password);

        return store;
    }

    public static Store getStore(String username, String password) throws MessagingException {
        return getStore(username, password, QUABBLY_HOST);
    }

    private static Store getStore(String username, String password, String host) throws MessagingException {
        String protocol = "imap";
        String port = "993";


        Properties properties = getServerProperties(protocol, host, port);
        Session session = Session.getDefaultInstance(properties);

        Store store = session.getStore(protocol);
        store.connect(QUABBLY_HOST, 993, username, password);

        return store;
    }

    public static Session getFromSMTPStore(String userId) {
        return smtpSessionStore.get(userId);
    }

    private static void closeFolder(Folder folder) {
        try {
            folder.getStore().close();
            System.out.println("close ok");
        } catch (MessagingException e) {
            System.out.println("error closing folder for user");
        }
    }

    private static void disconnectUser(String userId) {
        if (smtpSessionStore.get(userId) != null) {

            closeFolder(inboxFolder.get(userId));
            inboxFolder.remove(userId);

            closeFolder(sentFolder.get(userId));
            sentFolder.remove(userId);

            closeFolder(alertsFolder.get(userId));
            alertsFolder.remove(userId);

            closeFolder(newslettersFolder.get(userId));
            newslettersFolder.remove(userId);

            closeFolder(trashFolder.get(userId));
            trashFolder.remove(userId);

            closeFolder(spamFolder.get(userId));
            spamFolder.remove(userId);

            closeSession(smtpSessionStore.get(userId));
            smtpSessionStore.remove(userId);

        }
    }

    private static void closeSession(Session session) {
        try {
            session.getStore().close();
        } catch (MessagingException e) {
            System.out.println("error closing folder for user");
        }
    }

    private static void addToSentFolder(String userId, Store store) throws MessagingException {
        sentFolder.put(userId, getFolder(userId, store, "Sent"));
    }

    private static void addToInboxFolder(String userId, Store store) throws MessagingException {
        inboxFolder.put(userId, getFolder(userId, store, "INBOX"));
    }

    private static void addNewslettersFolder(String userId, Store store) throws MessagingException {
        newslettersFolder.put(userId, getFolder(userId, store, "Newsletters"));
    }

    private static void addToAlertsFolder(String userId, Store store) throws MessagingException {
        alertsFolder.put(userId, getFolder(userId, store, "Alerts"));
    }

    private static void addToSpamFolder(String userId, Store store) throws MessagingException {
        spamFolder.put(userId, getFolder(userId, store, "Spam"));
    }

    private static void addToTrashFolder(String userId, Store store) throws MessagingException {
        trashFolder.put(userId, getFolder(userId, store, "Trash"));
    }

    private static boolean userAlreadyExistsInMailStore(String userId) {
        return smtpSessionStore.get(userId) != null;
    }

    private static Folder getFolder(String userId, Store store, String folderName) throws MessagingException {
        Folder folder = store.getFolder(folderName);

//        // Add messageCountListener to listen for new messages
//        folder.addMessageCountListener(new MessageCountAdapter() {
//            public void messagesAdded(MessageCountEvent ev) {
//                System.out.println("user " +userId+ " has "+ ev.getMessages().length + " new message(s)");
//
//            }
//        });
        return folder;
    }

    public static Folder getFolderByName(String folderName, String userId) {
        switch (folderName) {
            case "INBOX":
                return getFromInboxFolder(userId);
            case "Sent":
                return getFromSentFolder(userId);
            case "Alerts":
                return getFromAlertsFolder(userId);
            case "Newsletters":
                return getFromNewslettersFolder(userId);
            case "Spam":
                return getFromSpamFolder(userId);
            case "Trash":
                return getFromTrashFolder(userId);
            default:
                return null;
        }
    }

    @Scheduled(fixedRate = 180000)
    public void setFolderIdle() {
        System.out.println("ready to ping server #idle");
        for (String userId : smtpSessionStore.keySet()) {
            try {

                inboxFolder.get(userId).getMessageCount();
                newslettersFolder.get(userId).getMessageCount();
                alertsFolder.get(userId).getMessageCount();
                sentFolder.get(userId).getMessageCount();
                spamFolder.get(userId).getMessageCount();
                trashFolder.get(userId).getMessageCount();


                System.out.println("completed  for user " + userId + " at " + new Date());
            } catch (MessagingException e) {
                System.out.println("problem with setting to idle " + e);
            }
        }
    }

    //very very bad code
    @Scheduled(fixedRate = 3600000)
    public void removeIdleConnections() {
        System.out.println("ready to remove #idle connections");
        for (String userId : timer.keySet()) {
            try {

                Calendar time = Calendar.getInstance();
                time.add(Calendar.MINUTE, -30);

                if(time.getTime().after(timer.get(userId))){

                    disconnectUser(userId);
                    System.out.println("user "+userId+" disconnected");
                }
            } catch (Exception e) {
                System.out.println("problem with removing idle connections" + e);
            }
        }
    }

    private static Properties getServerProperties(String protocol, String host,
                                                  String port) {
        Properties properties = new Properties();

        // server setting
        properties.put(String.format("mail.%s.host", protocol), host);
        properties.put(String.format("mail.%s.port", protocol), port);

        // SSL setting
        properties.setProperty(
                String.format("mail.%s.socketFactory.class", protocol),
                "javax.net.ssl.SSLSocketFactory");
        properties.setProperty(
                String.format("mail.%s.socketFactory.fallback", protocol),
                "false");
        properties.setProperty(
                String.format("mail.%s.socketFactory.port", protocol),
                String.valueOf(port));

        return properties;
    }
}
