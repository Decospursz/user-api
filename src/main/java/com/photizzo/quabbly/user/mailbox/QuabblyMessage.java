package com.photizzo.quabbly.user.mailbox;

import java.util.List;

public class QuabblyMessage {

    private String id;
    private String from;
    private String to;
    private String cc;
    private String subject;
    private String sendDate;
    private String content;
    private String contentType;
    private boolean seen;
    private List<MessagePartAttachment> attachments;

    QuabblyMessage(String id, String from, String to, String cc, String subject, String sendDate, String content, String contentType, List<MessagePartAttachment> attachments, boolean seen) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.cc = cc;
        this.subject = subject;
        this.sendDate = sendDate;
        this.content = content;
        this.contentType = contentType;
        this.attachments = attachments;
        this.seen = seen;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSendDate() {
        return sendDate;
    }

    public void setSendDate(String sendDate) {
        this.sendDate = sendDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public List<MessagePartAttachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<MessagePartAttachment> attachments) {
        this.attachments = attachments;
    }
}
