package com.photizzo.quabbly.user.mailbox;

import com.google.gson.Gson;
import com.sun.mail.util.BASE64DecoderStream;
import org.apache.commons.io.IOUtils;
import org.apache.commons.math3.util.Precision;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.Message.RecipientType;
import javax.mail.internet.*;
import javax.mail.search.MessageIDTerm;
import javax.mail.search.SearchTerm;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

public class EmailProcessor {

    public static List<String> markMailsAsRead(Folder folder, List<String> ids) throws MessagingException {
        List<String> marked = new ArrayList<>();


        for (String id : ids) {
            String realId = getRealMessageIDFromBase64EncodedVersion(id);

            Message[] message = searchForMessageById(folder, realId);
            if (message != null) {
                if (folder.isOpen()) {
                    folder.close(false);
                }
                folder.open(Folder.READ_WRITE);
                folder.setFlags(message, new Flags(Flags.Flag.SEEN), true);
                marked.add(id);
                folder.close(false);
            }
        }

        return marked;
    }

    private static Message[] searchForMessageById(Folder folder, String realId) throws MessagingException {
        SearchTerm term = new MessageIDTerm(realId);

        if (!folder.isOpen()) {
            folder.open(Folder.READ_ONLY);
        }
        Message[] msg = folder.search(term);

        return (msg != null && msg.length > 0) ? msg : null;
    }

    private static String getRealMessageIDFromBase64EncodedVersion(String id) {
        return new String(Base64.getDecoder().decode(id));
    }

    private MessagePart getTextFromMessage(Message message, boolean includeAttachmentContent) throws MessagingException, IOException {
        if (message.isMimeType("multipart/*")) {
            MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
            return getTextFromMimeMultipart(mimeMultipart, new MessagePart(), includeAttachmentContent);
        }

        return new MessagePart(message.getContent().toString());
    }

    private MessagePart getTextFromMimeMultipart(
            MimeMultipart mimeMultipart, MessagePart messagePart, boolean includeAttachmentContent) throws MessagingException, IOException {


        int count = mimeMultipart.getCount();
        for (int i = 0; i < count; i++) {
            BodyPart bodyPart = mimeMultipart.getBodyPart(i);
            if (Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition())) {

                MessagePartAttachment attachment = new MessagePartAttachment(bodyPart.getFileName(), getSize(bodyPart), bodyPart.getContentType());
                if (includeAttachmentContent) {
                    attachment.setRawSize((long) getAdjustedSize(bodyPart));
                    attachment.setContent(IOUtils.toByteArray(bodyPart.getInputStream()));
                }
                messagePart.getAttachments().add(attachment);
            } else if (bodyPart.isMimeType("text/plain")) {
                messagePart.setTextContent(messagePart.getTextContent() + bodyPart.getContent().toString());
            } else if (bodyPart.isMimeType("text/html")) {
                messagePart.setHtmlContent(messagePart.getHtmlContent() + bodyPart.getContent().toString());
            } else if (bodyPart.getContent() instanceof MimeMultipart) {
                getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent(), messagePart, includeAttachmentContent);
            } else if (bodyPart.getContent() instanceof BASE64DecoderStream && includeAttachmentContent) {
                BASE64DecoderStream base64DecoderStream = (BASE64DecoderStream) bodyPart.getContent();
                byte[] byteArray = IOUtils.toByteArray(base64DecoderStream);
                byte[] encodeBase64 = Base64.getEncoder().encode(byteArray);
                String content = MessageFormat.format("data:{0};base64, {1}", bodyPart.getContentType().split(";")[0], new String(encodeBase64, "UTF-8"));

                messagePart.addImage(content);
            }
        }

        return messagePart;
    }

    private double getAdjustedSize(BodyPart bodyPart) throws MessagingException {
        double offset = 0.765;
        return bodyPart.getSize() * offset;
    }

    private String getSize(BodyPart bodyPart) throws MessagingException {
        double mbDivisor = (1024d * 1024d);
        double kbDivisor = (1024d);
        double numerator = getAdjustedSize(bodyPart);

        if (numerator / mbDivisor > 1) {
            return String.format("%s MB", Precision.round(numerator / mbDivisor, 1));
        } else if (numerator / kbDivisor > 1) {
            return String.format("%s KB", Precision.round(numerator / kbDivisor, 1));
        } else {
            return "1 KB";
        }
    }


    public QuabblyMessageHolder download(Folder folder, Pageable pageable, boolean includeAttachmentContent) throws IOException {

        try {
            if (!folder.isOpen()) {
                folder.open(Folder.READ_ONLY);
            }
            // opens the inbox folder



//            // search for all "unseen" messages
//            Flags seen = new Flags(Flags.Flag.SEEN);
//            FlagTerm unseenFlagTerm = new FlagTerm(seen, false);
//            FlagTerm recentFlagTerm = new FlagTerm(new Flags(Flags.Flag.RECENT), true);
//
//            SearchTerm searchTerm = new AndTerm(unseenFlagTerm, recentFlagTerm);

            // fetches new messages from server
            //Message[] messages = folderInbox.search(recentFlagTerm);

            List<QuabblyMessage> qMessages = new ArrayList<>();

            int messageCount = folder.getMessageCount();
            int size = pageable.getPageSize();
            int pageCount = messageCount % size == 0 ? (messageCount / size) : (messageCount / size) + 1;
            int pageNo = (pageable.getPageNumber() < 1 || pageable.getPageNumber() > pageCount) ? 1 : pageable.getPageNumber();

            int start = (((pageNo - 1) > 0 ? (pageNo - 1) : 0) * size);
            int end = ((start + size) < messageCount ? (start + size) : messageCount) - 1;

            int reverseStart = (messageCount - end);
            int reverseEnd = messageCount - start;

            Message[] messages = folder.getMessages(reverseStart, reverseEnd);

            List<Message> messageList = Arrays.asList(messages);
            Collections.reverse(messageList);


            for (Message msg : messageList) {

                Address[] fromAddress = msg.getFrom();
                String from = decodeUTF8String(fromAddress[0].toString());
                String subject = decodeUTF8String(msg.getSubject());
                String toList = parseAddresses(msg
                        .getRecipients(RecipientType.TO));
                String ccList = parseAddresses(msg
                        .getRecipients(RecipientType.CC));
                String sentDate = msg.getSentDate() == null ? new Date().toString() :msg.getSentDate().toString();

                String contentType = msg.getContentType();
                String messageContent = "";

                String id;
                if(msg.getHeader("Message-ID") == null || msg.getHeader("Message-ID").length == 0){
                    id = msg.getSubject();
                    System.out.println("I found a message without a Message ID");
                    System.out.println("message was sent to "+ msg.getRecipients(RecipientType.TO)[0].toString() );
                    System.out.println("Message subject is "+msg.getSubject()+ "in folder "+folder.getName());
                }else{
                    id = msg.getHeader("Message-ID")[0];
                }

                String idEncrypted = Base64.getEncoder().encodeToString(id.getBytes("utf-8"));
                boolean seen = msg.isSet(Flags.Flag.SEEN);


                List<MessagePartAttachment> attachments = new ArrayList<>();

                MessagePart msgPart = getTextFromMessage(msg, includeAttachmentContent);
                if (msgPart != null) {
                    messageContent = msgPart.getHtmlContent().length() > 0 ? msgPart.getHtmlContent() : msgPart.getTextContent();
                    attachments = msgPart.getAttachments();

                    for (String img : msgPart.getImages()) {
                        messageContent = messageContent.replaceFirst("src=\"cid(\\S+)\"", String.format("src=\"%s\" ", img));
                    }
                }

                QuabblyMessage m = new QuabblyMessage(idEncrypted, from, toList, ccList, subject, sentDate, messageContent, contentType, attachments, seen);
                qMessages.add(m);
                if(msg.getSentDate() == null){
                    System.out.println("weird message "+new Gson().toJson(m));
                }
            }

            return new QuabblyMessageHolder(qMessages, messageCount, pageCount);


        } catch (NoSuchProviderException ex) {
            System.out.println("No provider for protocol: ");
            ex.printStackTrace();
        } catch (MessagingException ex) {
            System.out.println("Could not connect to the message store");
            ex.printStackTrace();
        }

        return null;
    }

    private String decodeUTF8String(String string) {
        try{
            return MimeUtility.decodeWord(string.replaceAll("\"", ""));
        } catch (ParseException | UnsupportedEncodingException e) {
            return string;
        }
    }

    private String parseAddresses(Address[] address) {

        if (address != null) {
            return Arrays.stream(address).map(Object::toString).collect(Collectors.joining(","));
        }

        return "";
    }

    public boolean sendEmail(Session session, Folder folder, String uploadDir, String fromEmail, String fromName, String[] recipients, String[] cc, String[] bcc, String subject, String body, MultipartFile[] attachments) {
        try {
            MimeMessage msg = new MimeMessage(session);
            //set message headers
            msg.addHeader("format", "flowed");
            msg.addHeader("Content-Transfer-Encoding", "8bit");

            msg.setFrom(new InternetAddress(fromEmail, fromName));

            msg.setReplyTo(InternetAddress.parse(fromEmail, false));

            msg.setSubject(subject, "UTF-8");

            for (String recipient : recipients) {
                msg.addRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient, false));
            }

            for (String recipient : cc) {
                msg.addRecipients(Message.RecipientType.CC, InternetAddress.parse(recipient, false));
            }

            for (String recipient : bcc) {
                msg.addRecipients(RecipientType.BCC, InternetAddress.parse(recipient, false));
            }

            msg.setSentDate(new Date());


            BodyPart messageBodyPart = new MimeBodyPart();

            // Now set the actual message
            messageBodyPart.setContent(body, "text/html; charset=utf-8");


            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            for(MultipartFile uploadedFile : attachments) {
                String filePath = uploadDir + uploadedFile.getOriginalFilename();
                File file = new File(filePath);
                uploadedFile.transferTo(file);

                DataSource source = new FileDataSource(filePath);

                messageBodyPart = new MimeBodyPart();
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(uploadedFile.getOriginalFilename());
                multipart.addBodyPart(messageBodyPart);

            }


            // Send the complete message parts
            msg.setContent(multipart);

            Transport.send(msg);

            saveAsSentItem(folder, msg);

            deleteFiles(attachments, uploadDir);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void deleteFiles(MultipartFile[] attachments, String fileDir) {
        for(MultipartFile uploadedFile : attachments) {
            String filePath = fileDir + uploadedFile.getOriginalFilename();
            File file = new File(filePath);
            file.delete();
        }
    }

    private void saveAsSentItem(Folder folder, MimeMessage msg) throws MessagingException {
        if(!folder.exists()){
            folder.create(Folder.HOLDS_MESSAGES);
        }

        if (!folder.isOpen()) {
            folder.open(Folder.READ_WRITE);
        }

        msg.setFlag(Flags.Flag.SEEN, true);

        folder.appendMessages(new Message[]{msg});

    }
}