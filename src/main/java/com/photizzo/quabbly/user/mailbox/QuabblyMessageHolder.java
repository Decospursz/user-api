package com.photizzo.quabbly.user.mailbox;

import java.util.List;

public class QuabblyMessageHolder {

    private List<QuabblyMessage> messages;
    private int totalMessages;
    private int pageCount;

    public QuabblyMessageHolder() {
    }

    public QuabblyMessageHolder(List<QuabblyMessage> messages, int totalMessages, int pageCount) {
        this.messages = messages;
        this.totalMessages = totalMessages;
        this.pageCount = pageCount;
    }

    public List<QuabblyMessage> getMessages() {
        return messages;
    }

    public void setMessages(List<QuabblyMessage> messages) {
        this.messages = messages;
    }

    public int getTotalMessages() {
        return totalMessages;
    }

    public void setTotalMessages(int totalMessages) {
        this.totalMessages = totalMessages;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }
}
