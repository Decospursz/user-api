package com.photizzo.quabbly.user.mailbox;

import org.apache.commons.codec.digest.DigestUtils;

public class MessagePartAttachment {

    private String name;
    private long rawSize;
    private String size;
    private String contentType;
    private String id;
    private byte[] content;

    public MessagePartAttachment() {
    }

    MessagePartAttachment(String name, String size, String contentType) {
        this.name = name;
        this.size = size;
        this.id = DigestUtils.md5Hex(name);
        this.contentType = contentType;
    }

    public long getRawSize() {
        return rawSize;
    }

    public void setRawSize(long rawSize) {
        this.rawSize = rawSize;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "MessagePartAttachment{" +
                "name='" + name + '\'' +
                ", size='" + size + '\'' +
                '}';
    }
}
