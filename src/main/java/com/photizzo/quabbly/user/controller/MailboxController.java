package com.photizzo.quabbly.user.controller;

import com.photizzo.quabbly.user.dto.enums.Status;
import com.photizzo.quabbly.user.dto.input.ApiFieldError;
import com.photizzo.quabbly.user.dto.input.MarkAsReadInputDto;
import com.photizzo.quabbly.user.dto.input.SendEmailInputDTO;
import com.photizzo.quabbly.user.dto.output.*;
import com.photizzo.quabbly.user.service.MailboxService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/v1/mail")
@ApiResponses({
        @ApiResponse(code = 400, message = "Bad request", response = ApiFieldError[].class),
})
public class MailboxController extends Controller {

    private static final int ITEMS_PER_PAGE = 10;

    @Autowired
    private MailboxService mailboxService;

    @GetMapping("/inbox")
    public MailboxResponseDTO inbox(@PageableDefault(value = ITEMS_PER_PAGE) Pageable pageable) {
        MailboxResponseDTO response = mailboxService.fetchInbox(pageable);
        updateHttpStatus(response);
        return response;
    }

    @GetMapping("/newsletters")
    public MailboxResponseDTO newsletters(@PageableDefault(value = ITEMS_PER_PAGE) Pageable pageable) {
        MailboxResponseDTO response = mailboxService.fetchNewsletters(pageable);
        updateHttpStatus(response);
        return response;
    }

    @GetMapping("/alerts")
    public MailboxResponseDTO alerts(@PageableDefault(value = ITEMS_PER_PAGE) Pageable pageable) {
        MailboxResponseDTO response = mailboxService.fetchAlerts(pageable);
        updateHttpStatus(response);
        return response;
    }

    @GetMapping("/spam")
    public MailboxResponseDTO spam(@PageableDefault(value = ITEMS_PER_PAGE) Pageable pageable) {
        MailboxResponseDTO response = mailboxService.fetchSpam(pageable);
        updateHttpStatus(response);
        return response;
    }

    @GetMapping("/trash")
    public MailboxResponseDTO trash(@PageableDefault(value = ITEMS_PER_PAGE) Pageable pageable) {
        MailboxResponseDTO response = mailboxService.fetchTrash(pageable);
        updateHttpStatus(response);
        return response;
    }

    @PostMapping("/mark_as_read")
    public MarkMailAsReadResponseDto markAsRead(@RequestBody @Valid MarkAsReadInputDto dto) {

        MarkMailAsReadResponseDto response = mailboxService.markAsRead(dto);
        updateHttpStatus(response);
        return response;
    }

    @RequestMapping(value = "/send", method = RequestMethod.POST, consumes = "multipart/form-data")
    public StandardResponseDTO sendEmail(@Valid SendEmailInputDTO dto, BindingResult result) {

        StandardResponseDTO response = mailboxService.sendEmail(dto, result);
        updateHttpStatus(response);
        return response;
    }

    @GetMapping("/download_file/{id}")
    public ResponseEntity<byte[]> download(@PathVariable("id") String id, @PageableDefault(value = ITEMS_PER_PAGE) Pageable pageable) {
        DownloadResponseDTO response = mailboxService.downloadAttachment(id, pageable);
        if (response.getStatus() == Status.SUCCESS) {
            byte[] contents = response.getAttachment().getContent();

            HttpHeaders headers = new HttpHeaders();
            headers.setContentLength(contents.length);
            headers.setContentType(MediaType.parseMediaType(response.getAttachment().getContentType()));

            String filename = response.getAttachment().getName();
            headers.setContentDispositionFormData(filename, filename);
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
            return new ResponseEntity<>(contents, headers, HttpStatus.OK);

        }
        updateHttpStatus(response);

        return null;
    }

    @GetMapping("/sent")
    public SentMailResponseDTO sent(@PageableDefault(value = ITEMS_PER_PAGE) Pageable pageable) {
        SentMailResponseDTO response = mailboxService.fetchSentMails(pageable);
        updateHttpStatus(response);
        return response;
    }

}
