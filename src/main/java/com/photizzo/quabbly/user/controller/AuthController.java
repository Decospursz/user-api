package com.photizzo.quabbly.user.controller;

import com.photizzo.quabbly.user.dto.input.LoginInputDTO;
import com.photizzo.quabbly.user.dto.input.RegisterAdminUserInputDto;
import com.photizzo.quabbly.user.dto.output.LoginResponseDTO;
import com.photizzo.quabbly.user.dto.output.StandardResponseDTO;
import com.photizzo.quabbly.user.dto.output.UserResponseDTO;
import com.photizzo.quabbly.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/v1/auth")
public class AuthController extends Controller {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private HttpServletResponse response;

    @PostMapping("/login")
    public LoginResponseDTO login(@RequestBody LoginInputDTO loginUser) {
        LoginResponseDTO serviceResponse = userService.loginUser(loginUser, authenticationManager);
        updateHttpStatus(serviceResponse);
        return serviceResponse;
    }

    @PostMapping("/register")
    public UserResponseDTO register(@RequestBody @Valid RegisterAdminUserInputDto registerUserDto) {
        UserResponseDTO serviceResponse = userService.registerAdminUser(registerUserDto);
        updateHttpStatus(serviceResponse);
        return serviceResponse;
    }

    @PostMapping("/validate")
    public StandardResponseDTO validate(@RequestHeader("x-user-email") String email, @RequestHeader("x-user-token") String token) {
        StandardResponseDTO serviceResponse = userService.validateUserToken(email, token);
        updateHttpStatus(serviceResponse);
        return serviceResponse;
    }

}
