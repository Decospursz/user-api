package com.photizzo.quabbly.user.controller;

import com.photizzo.quabbly.user.dto.enums.Status;
import com.photizzo.quabbly.user.dto.input.*;
import com.photizzo.quabbly.user.dto.input.bizReg.BusinessNameInputDTO;
import com.photizzo.quabbly.user.dto.input.bizReg.CheckNameInputDTO;
import com.photizzo.quabbly.user.dto.input.bizReg.CompanyRegInputDTO;
import com.photizzo.quabbly.user.dto.input.bizReg.NameCheckResponseInputDTO;
import com.photizzo.quabbly.user.dto.output.AllUsersResponseDTO;
import com.photizzo.quabbly.user.dto.output.BasicResponseDTO;
import com.photizzo.quabbly.user.dto.output.StandardResponseDTO;
import com.photizzo.quabbly.user.dto.output.UserResponseDTO;
import com.photizzo.quabbly.user.dto.output.bizReg.BusinessNameResponseDTO;
import com.photizzo.quabbly.user.dto.output.bizReg.CheckNameListResponseDTO;
import com.photizzo.quabbly.user.dto.output.bizReg.CheckNameResponseDTO;
import com.photizzo.quabbly.user.dto.output.bizReg.CompanyRegResponseDTO;
import com.photizzo.quabbly.user.service.UserService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@RequestMapping("/v1/users")
@ApiResponses({
        @ApiResponse(code = 400, message = "Bad request", response = ApiFieldError[].class),
})
@CrossOrigin
public class UserController extends Controller {

    @Autowired
    private UserService userService;


    @GetMapping("/all")
    public AllUsersResponseDTO fetchUsersInCompany() {
        AllUsersResponseDTO serviceResponse = userService.fetchAllUsersInCompany();
        updateHttpStatus(serviceResponse);
        return serviceResponse;
    }

    @GetMapping("/{userId}")
    public UserResponseDTO fetchUserInCompanyByUuid(@PathVariable("userId") String userId ) {
        UserResponseDTO serviceResponse = userService.fetchUserInCompany(userId);
        updateHttpStatus(serviceResponse);
        return serviceResponse;
    }

    @GetMapping("/id/{userId}")
    public UserResponseDTO fetchUserInCompanyById(@PathVariable("userId") String userId ) {
        UserResponseDTO serviceResponse = userService.fetchUserInCompanyById(userId);
        updateHttpStatus(serviceResponse);
        return serviceResponse;
    }


    @GetMapping("/roles")
    public BasicResponseDTO listRoles() {
        return new BasicResponseDTO(Status.SUCCESS, userService.fetchRoles());
    }

    @PutMapping("/{userId}")
    public UserResponseDTO updateUser(@RequestBody @Valid UpdateUserInputDto updateUserInputDto, @PathVariable("userId") String userId ,HttpServletRequest request, HttpServletResponse response) {
        UserResponseDTO serviceResponse = userService.updateUser(updateUserInputDto, userId);
        updateHttpStatus(serviceResponse);
        return serviceResponse;
    }

    @PutMapping("/password")
    public UserResponseDTO updatePassword(@RequestBody @Valid PasswordDto passwordDto, HttpServletRequest request, HttpServletResponse response) {
        UserResponseDTO serviceResponse = userService.updatePassword(passwordDto, request.getUserPrincipal().getName());
        updateHttpStatus(serviceResponse);
        return serviceResponse;
    }

    @PostMapping("/add")
    public UserResponseDTO addUser(@RequestBody @Valid RegisterUserInputDto registerUserDto) {
        UserResponseDTO serviceResponse = userService.registerUser(registerUserDto);
        updateHttpStatus(serviceResponse);
        return serviceResponse;
    }

    @DeleteMapping("/{userId}/delete")
    public UserResponseDTO deleteUser(@PathVariable("userId") String userId){
        UserResponseDTO serviceResponse = userService.deleteUser(userId);
        updateHttpStatus(serviceResponse);
        return serviceResponse;
    }

    @PutMapping("/migrate")
    public UserResponseDTO migrateUser(@RequestBody @Valid MigrateUserInputDTO migrateUserInputDTO) {
        UserResponseDTO serviceResponse = userService.migrateUser(migrateUserInputDTO);
        updateHttpStatus(serviceResponse);
        return  serviceResponse;
    }

    @PutMapping("/alias")
    public UserResponseDTO addAlias(@RequestBody @Valid String email){
        UserResponseDTO serviceResponse = userService.addAlias(email);
        updateHttpStatus(serviceResponse);
        return serviceResponse;
    }

    @PostMapping("/register-business")
    public BusinessNameResponseDTO registerBusinessName(@RequestBody @Valid BusinessNameInputDTO businessNameInputDTO){
        BusinessNameResponseDTO serviceResponse= userService.registerBusinessName(businessNameInputDTO);
        updateHttpStatus(serviceResponse);
        return serviceResponse;
    }


    @PostMapping("/register-company")
    public CompanyRegResponseDTO registerCompany(@RequestBody @Valid CompanyRegInputDTO companyRegInputDTO){
        CompanyRegResponseDTO serviceResponse= userService.registerCompany(companyRegInputDTO);
        updateHttpStatus(serviceResponse);
        return serviceResponse;
    }


    @PostMapping("/check-name")
    public CheckNameResponseDTO checkName(@RequestBody @Valid CheckNameInputDTO dto){
        CheckNameResponseDTO serviceResponse= userService.checkNameAvailability(dto);
        updateHttpStatus(serviceResponse);
        return serviceResponse;
    }

    @GetMapping("/check-name/all")
    public CheckNameListResponseDTO fetchNameAvailabilityCheckList(){
        CheckNameListResponseDTO serviceResponse = userService.fetchNameAvailabilityCheckList();
        updateHttpStatus(serviceResponse);
        return serviceResponse;
    }


    @PostMapping("/check-name/{id}/response")
    public StandardResponseDTO nameCheckResponse(@PathVariable("id") String checkNameId, @RequestBody @Valid NameCheckResponseInputDTO dto){
        StandardResponseDTO serviceResponse = userService.nameCheckResponse(checkNameId, dto);
        updateHttpStatus(serviceResponse);
        return serviceResponse;
    }

    @GetMapping("/check-name/{id}")
    public CheckNameResponseDTO fetchNameAvailabilityCheckRecord(@PathVariable("id") String checkNameId){
        CheckNameResponseDTO serviceResponse= userService.fetchNameAvailabilityRecord(checkNameId);
        updateHttpStatus(serviceResponse);
        return serviceResponse;
    }



}
