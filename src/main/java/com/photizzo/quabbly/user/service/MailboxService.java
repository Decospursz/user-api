/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.photizzo.quabbly.user.service;

import com.photizzo.quabbly.user.dto.input.MarkAsReadInputDto;
import com.photizzo.quabbly.user.dto.input.SendEmailInputDTO;
import com.photizzo.quabbly.user.dto.output.*;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

/**
 * @author osita
 */
@Service
public interface MailboxService {

    MailboxResponseDTO fetchInbox(Pageable pageable);

    MailboxResponseDTO fetchNewsletters(Pageable pageable);

    MailboxResponseDTO fetchAlerts(Pageable pageable);

    MailboxResponseDTO fetchSpam(Pageable pageable);

    MailboxResponseDTO fetchTrash(Pageable pageable);

    SentMailResponseDTO fetchSentMails(Pageable pageable);

    StandardResponseDTO sendEmail(SendEmailInputDTO sendEmailInputDTO, BindingResult result);

    DownloadResponseDTO downloadAttachment(String id, Pageable pageable);

    MarkMailAsReadResponseDto markAsRead(MarkAsReadInputDto dto);
}
