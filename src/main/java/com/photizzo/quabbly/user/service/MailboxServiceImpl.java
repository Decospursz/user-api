package com.photizzo.quabbly.user.service;

import com.photizzo.quabbly.user.dto.enums.Status;
import com.photizzo.quabbly.user.dto.input.MarkAsReadInputDto;
import com.photizzo.quabbly.user.dto.input.SendEmailInputDTO;
import com.photizzo.quabbly.user.dto.output.*;
import com.photizzo.quabbly.user.mailbox.*;
import com.photizzo.quabbly.user.model.migration.Migration;
import com.photizzo.quabbly.user.repository.MigrationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import javax.mail.Folder;
import javax.mail.Session;
import java.util.List;
import java.util.Optional;


@Service("mailboxService")
public class MailboxServiceImpl extends AbstractService implements MailboxService {

    private static final String SERVICE_NAME = "MailboxService";
    private static final String FOLDER_INBOX = "INBOX";
    private static final String FOLDER_SENT = "Sent";
    private static final String FOLDER_NEWSLETTERS = "Newsletters";
    private static final String FOLDER_ALERTS = "Alerts";
    private static final String FOLDER_TRASH = "Trash";
    private static final String FOLDER_SPAM = "Spam";

    @Autowired
    private Environment env;

    @Autowired
    private Validator validator;

    @Autowired
    private MigrationRepository migrationRepository;


    @Override
    public MailboxResponseDTO fetchInbox(Pageable pageable) {
        try {
            Folder folder = MailSessionHolder.getFromInboxFolder(getUserId());
            if (folder == null) {
                return new MailboxResponseDTO(Status.FAILED_VALIDATION);
            }

            return new MailboxResponseDTO(Status.SUCCESS, new EmailProcessor().download(folder, pageable, false));


        } catch (Exception ex) {
            logError(SERVICE_NAME, "fetchInbox", getUserId(), ex);
            return new MailboxResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public MailboxResponseDTO fetchNewsletters(Pageable pageable) {
        try {
            Folder folder = MailSessionHolder.getFromNewslettersFolder(getUserId());
            if (folder == null) {
                return new MailboxResponseDTO(Status.FAILED_VALIDATION);
            }


            return new MailboxResponseDTO(Status.SUCCESS, new EmailProcessor().download(folder, pageable, false));


        } catch (Exception ex) {
            logError(SERVICE_NAME, "fetchNewsletters", getUserId(), ex);
            return new MailboxResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public MailboxResponseDTO fetchAlerts(Pageable pageable) {
        try {
            Folder folder = MailSessionHolder.getFromAlertsFolder(getUserId());
            if (folder == null) {
                return new MailboxResponseDTO(Status.FAILED_VALIDATION);
            }

            return new MailboxResponseDTO(Status.SUCCESS, new EmailProcessor().download(folder, pageable, false));


        } catch (Exception ex) {
            logError(SERVICE_NAME, "fetchAlerts", getUserId(), ex);
            return new MailboxResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public MailboxResponseDTO fetchSpam(Pageable pageable) {
        try {
            Folder folder = MailSessionHolder.getFromSpamFolder(getUserId());
            if (folder == null) {
                return new MailboxResponseDTO(Status.FAILED_VALIDATION);
            }

            return new MailboxResponseDTO(Status.SUCCESS, new EmailProcessor().download(folder, pageable, false));


        } catch (Exception ex) {
            logError(SERVICE_NAME, "fetchSpam", getUserId(), ex);
            return new MailboxResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public MailboxResponseDTO fetchTrash(Pageable pageable) {
        try {
            Folder folder = MailSessionHolder.getFromTrashFolder(getUserId());
            if (folder == null) {
                return new MailboxResponseDTO(Status.FAILED_VALIDATION);
            }

            return new MailboxResponseDTO(Status.SUCCESS, new EmailProcessor().download(folder, pageable, false));


        } catch (Exception ex) {
            logError(SERVICE_NAME, "fetchTrash", getUserId(), ex);
            return new MailboxResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public StandardResponseDTO sendEmail(SendEmailInputDTO dto, BindingResult result) {

        try {
            if (result.hasErrors()) {
                return new StandardResponseDTO(Status.FAILED_VALIDATION, getValidationErrors(result));
            }

            Session session = MailSessionHolder.getFromSMTPStore(getUserId());
            Folder folder = MailSessionHolder.getFromSentFolder(getUserId());

            if (session == null || folder == null) {
                return new StandardResponseDTO(Status.FAILED_VALIDATION);
            }

            String uploadDir = "/tmp/";
            boolean sent = new EmailProcessor().sendEmail(session, folder, uploadDir, getUserEmail(), getUsername(), dto.getRecipients().split(","), dto.getCc().split(","), dto.getBcc().split(","), dto.getSubject(), dto.getContent(), dto.getFiles());

            if (!sent) {
                return new StandardResponseDTO(Status.INTERNAL_ERROR);
            }


            return new StandardResponseDTO(Status.SUCCESS);
        } catch (Exception ex) {
            logError(SERVICE_NAME, "sendEmail", getUserId(), ex);
            return new MailboxResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public DownloadResponseDTO downloadAttachment(String id, Pageable pageable) {
        try {
            Folder folder = MailSessionHolder.getFromInboxFolder(getUserId());
            if (folder == null) {
                return new DownloadResponseDTO(Status.FAILED_VALIDATION);
            }

            QuabblyMessageHolder message = new EmailProcessor().download(folder, pageable, true);
            if (message == null) {
                return new DownloadResponseDTO(Status.NOT_FOUND);
            }

            QuabblyMessage messageWithAttachments = message.getMessages().get(0);

            Optional<MessagePartAttachment> optionalMessagePartAttachment = messageWithAttachments.getAttachments().stream().filter(e -> e.getId().equals(id)).findFirst();

            if (!optionalMessagePartAttachment.isPresent()) {
                return new DownloadResponseDTO(Status.NOT_FOUND);
            }

            MessagePartAttachment attachment = optionalMessagePartAttachment.get();

            return new DownloadResponseDTO(Status.SUCCESS, attachment);


        } catch (Exception ex) {
            logError(SERVICE_NAME, "fetchEmails", getUserId(), ex);
            return new DownloadResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public MarkMailAsReadResponseDto markAsRead(MarkAsReadInputDto dto) {
        try {
            Folder folder = MailSessionHolder.getFolderByName(dto.getFolderName(), getUserId());
            if (folder == null) {
                return new MarkMailAsReadResponseDto(Status.FAILED_VALIDATION);
            }

            List<String> markedMails = EmailProcessor.markMailsAsRead(folder, dto.getIds());

            return new MarkMailAsReadResponseDto(Status.SUCCESS, markedMails);

        } catch (Exception ex) {
            logError(SERVICE_NAME, "markAsRead", getUserId(), ex);
            return new MarkMailAsReadResponseDto(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public SentMailResponseDTO fetchSentMails(Pageable pageable) {
        try {
            Folder folder = MailSessionHolder.getFromSentFolder(getUserId());
            if (folder == null) {
                return new SentMailResponseDTO(Status.FAILED_VALIDATION);
            }

            return new SentMailResponseDTO(Status.SUCCESS, new EmailProcessor().download(folder, pageable, false));


        } catch (Exception ex) {
            logError(SERVICE_NAME, "fetchSentMails", getUserId(), ex);
            return new SentMailResponseDTO(Status.INTERNAL_ERROR);
        }
    }
}
