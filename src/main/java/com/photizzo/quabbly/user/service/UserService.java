package com.photizzo.quabbly.user.service;

import com.photizzo.quabbly.user.config.TokenProvider;
import com.photizzo.quabbly.user.dto.enums.CheckNameStatus;
import com.photizzo.quabbly.user.dto.enums.Status;
import com.photizzo.quabbly.user.dto.input.*;
import com.photizzo.quabbly.user.dto.input.bizReg.*;
import com.photizzo.quabbly.user.dto.output.AllUsersResponseDTO;
import com.photizzo.quabbly.user.dto.output.LoginResponseDTO;
import com.photizzo.quabbly.user.dto.output.StandardResponseDTO;
import com.photizzo.quabbly.user.dto.output.UserResponseDTO;
import com.photizzo.quabbly.user.dto.output.bizReg.BusinessNameResponseDTO;
import com.photizzo.quabbly.user.dto.output.bizReg.CheckNameListResponseDTO;
import com.photizzo.quabbly.user.dto.output.bizReg.CheckNameResponseDTO;
import com.photizzo.quabbly.user.dto.output.bizReg.CompanyRegResponseDTO;
import com.photizzo.quabbly.user.mailbox.MailSessionHolder;
import com.photizzo.quabbly.user.model.Organisation;
import com.photizzo.quabbly.user.model.User;
import com.photizzo.quabbly.user.model.bizReg.*;
import com.photizzo.quabbly.user.model.mailbox.VirtualDomain;
import com.photizzo.quabbly.user.model.mailbox.VirtualUser;
import com.photizzo.quabbly.user.model.migration.Migration;
import com.photizzo.quabbly.user.model.migration.MigrationFolders;
import com.photizzo.quabbly.user.repository.CheckNameRepository;
import com.photizzo.quabbly.user.repository.MigrationRepository;
import com.photizzo.quabbly.user.repository.OrganisationRepository;
import com.photizzo.quabbly.user.repository.UserRepository;
import com.photizzo.quabbly.user.repository.bizReg.BusinessNameRepository;
import com.photizzo.quabbly.user.repository.bizReg.CompanyRegRepository;
import com.photizzo.quabbly.user.repository.mailbox.VirtualDomainRepository;
import com.photizzo.quabbly.user.repository.mailbox.VirtualUserRepository;
import com.photizzo.quabbly.user.repository.redis.SessionCacheManager;
import com.photizzo.quabbly.user.util.MailGenerator;
import org.apache.commons.codec.digest.Crypt;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Store;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;


@Service(value = "userService")
public class UserService extends AbstractService implements UserDetailsService {

    private static final String SERVICE_NAME = "UserService";

    @Autowired
    private SessionCacheManager sessionCacheManager;

    @Autowired
    private UserRepository userRepository;


    @Autowired
    private BusinessNameRepository businessNameRepository;

    @Autowired
    private CompanyRegRepository companyRegRepository;

    @Autowired
    private CheckNameRepository checkNameRepository;


    @Autowired
    MailGenerator mailGenerator = new MailGenerator();

    @Autowired
    private OrganisationRepository organisationRepository;

    @Autowired
    private TokenProvider tokenProvider;

    @Autowired
    private Environment env;

    @Autowired
    private VirtualDomainRepository virtualDomainRepository;

    @Autowired
    private VirtualUserRepository virtualUserRepository;

    @Autowired
    private MigrationRepository migrationRepository;




    @Autowired
    private BCryptPasswordEncoder bcryptEncoder;

    @Value("${user.roles}")
    private String roles;

    @Value("${user.role.admin}")
    private String adminRole;

    @Value("${user.roles.default}")
    private String defaultRoles;

    @Value("${allowed.domains}")
    private String allowedDomains;






    Logger log = LogManager.getLogger(UserService.class);


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(username);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), getAuthority(user));
    }

    private Set<SimpleGrantedAuthority> getAuthority(User user) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        for (String role : user.getRoles()) {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + role));
        }
        return authorities;
    }

    public AllUsersResponseDTO fetchAllUsersInCompany() {
        try {
            return new AllUsersResponseDTO(Status.SUCCESS, userRepository.findByOrganisationId(getTenantId()));
        } catch (Exception ex) {
            logError(SERVICE_NAME, "fetchAllUsersInCompany", getUserId(), ex);
            return new AllUsersResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    public UserResponseDTO fetchUserInCompany(String uuid) {
        try {
            Optional<User> user = userRepository.findByUuidAndOrganisationId(uuid, getTenantId());
            if(!user.isPresent()){
                return new UserResponseDTO(Status.NOT_FOUND);
            }
            return new UserResponseDTO(Status.SUCCESS, user.get());
        } catch (Exception ex) {
            logError(SERVICE_NAME, "fetchUserInCompany", getUserId(), ex);
            return new UserResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    public UserResponseDTO fetchUserInCompanyById(String id) {
        try {
            Optional<User> user = userRepository.findByIdAndOrganisationId(id, getTenantId());
            if(!user.isPresent()){
                return new UserResponseDTO(Status.NOT_FOUND);
            }
            return new UserResponseDTO(Status.SUCCESS, user.get());
        } catch (Exception ex) {
            logError(SERVICE_NAME, "fetchUserInCompany", getUserId(), ex);
            return new UserResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    private User findOne(String username) {
        return userRepository.findByEmail(username);
    }

    public LoginResponseDTO loginUser(LoginInputDTO loginUser, AuthenticationManager authenticationManager) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginUser.getUsername(),
                        loginUser.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);

        User u = findOne(loginUser.getUsername());

        try {
            String token = tokenProvider.generateToken(authentication, u);

            if (isAllowedDomain(getDomainNameFromEmail(u.getEmail()))) {
                MailSessionHolder.login(u.getUuid(), loginUser.getUsername(), loginUser.getPassword());
            }

            storeTokenInRedis(u.getEmail(), token);

            return new LoginResponseDTO(Status.SUCCESS, token, u);
        } catch (Exception e) {
            logError(SERVICE_NAME, "loginUser", null, e);
            return new LoginResponseDTO(Status.INTERNAL_ERROR);
        }

    }

    private void storeTokenInRedis(String email, String token) {
        sessionCacheManager.storeTokenInCache(email, token);
    }

    private <T> List<T> getExisting(List<T> t) {
        return t == null ? new ArrayList() : t;
    }

    @Transactional
    public UserResponseDTO registerAdminUser(RegisterAdminUserInputDto user) {
        try {

            Organisation o = new Organisation();
            o.setName(user.getCompanyName());
            o.setUuid(UUID.randomUUID().toString());
            o.setCreatedDate(new Date());


            organisationRepository.save(o);

            User newUser = new User();
            newUser.setEmail(user.getEmail());
            newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
            newUser.setFirstname(user.getFirstname());
            newUser.setLastname(user.getLastname());
            newUser.setCreatedDate(new Date());
            newUser.setRoles(Collections.singletonList(adminRole));
            newUser.setCreatedDate(new Date());
            newUser.setCreatedBy(null);
            newUser.setPendingPasswordUpdate(Boolean.TRUE);
            newUser.setUuid(UUID.randomUUID().toString());
            newUser.setOrganisationId(o.getUuid());


            userRepository.save(newUser);

            sendConfirmationMail(user.getEmail(), user.getFirstname());

            setupMailbox(user.getEmail(), user.getPassword(), true);


            return new UserResponseDTO(Status.SUCCESS, newUser);
        } catch (Exception e) {
            logError(SERVICE_NAME, "registerAdminUser", user.getEmail(), e);
            return new UserResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    private void sendConfirmationMail(String email, String firstName) {
        String from = "support@quabbly.com";
        String content = mailGenerator.generateMailHtml(firstName);
        String subject = "Verification of email";
        List<String> recipient = new ArrayList<>();
        recipient.add(email);
        sendMail(from, content, subject, recipient);
    }


    private void setupMailbox(String email, String password, boolean newDomain) {

        String prop = env.getRequiredProperty("mailbox.enabled");
        if(!prop.equals("enabled")){
            System.out.println("mailbox disabled, returning since prop is "+prop);
            return;
        }

        String domainName = getDomainNameFromEmail(email);

        String salt = "$6$Tq6ua0B97vxqNtQ9";

        if (!isAllowedDomain(domainName)) {
            return;
        }

        VirtualDomain virtualDomain = getDomain(domainName, newDomain);

        if (virtualDomain == null) {
            return;
        }

        VirtualUser existingUser = virtualUserRepository.findByEmail(email);

        if (existingUser != null) {
            existingUser.setPassword(Crypt.crypt(password, salt));
            virtualUserRepository.save(existingUser);
        } else {
            VirtualUser virtualUser = new VirtualUser();
            virtualUser.setDomainId(virtualDomain.getId());
            virtualUser.setEmail(email);
            virtualUser.setPassword(Crypt.crypt(password, salt));
            virtualUserRepository.save(virtualUser);
        }

    }

    private String getDomainNameFromEmail(String email) {
        return email.split("@")[1];
    }

    private VirtualDomain getDomain(String domainName, boolean newDomain) {
        VirtualDomain existingDomain = virtualDomainRepository.findByName(domainName);

        if (existingDomain != null && newDomain) {
            return null;
        }

        VirtualDomain virtualDomain;

        if (existingDomain != null) {
            virtualDomain = existingDomain;
        } else {
            virtualDomain = new VirtualDomain();
            virtualDomain.setName(domainName);
            virtualDomainRepository.save(virtualDomain);

        }

        if (virtualDomain.getId() != null) {
            return virtualDomain;
        }

        return null;
    }

    private boolean isAllowedDomain(String domainName) {
        return Arrays.asList(allowedDomains.split(",")).contains(domainName);
    }

    public UserResponseDTO registerUser(RegisterUserInputDto user) {
        try {

            if(!isAdminUser()){
                return new UserResponseDTO(Status.FORBIDDEN);
            }

            User newUser = new User();
            newUser.setEmail(user.getEmail());
            newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
            newUser.setFirstname(user.getFirstname());
            newUser.setLastname(user.getLastname());
            newUser.setCreatedDate(new Date());
            newUser.setRoles(Arrays.asList(defaultRoles.split(",")));
            newUser.setCreatedDate(new Date());
            newUser.setCreatedBy(null);
            newUser.setPendingPasswordUpdate(Boolean.TRUE);
            newUser.setUuid(UUID.randomUUID().toString());
            newUser.setOrganisationId(getTenantId());

            Optional<Organisation> o =  organisationRepository.findById(getTenantId());
            if(!o.isPresent()){
                return new UserResponseDTO(Status.NOT_FOUND);
            }

            Organisation organisation = o.get();
            int numberOfExistingUsersInOrg = organisation.getNumberOfExistingUsers();

            organisation.setNumberOfExistingUsers(numberOfExistingUsersInOrg + 1);

            userRepository.save(newUser);

            setupMailbox(user.getEmail(), user.getPassword(), false);


            return new UserResponseDTO(Status.SUCCESS, newUser);
        } catch (Exception e) {
            logError(SERVICE_NAME, "registerUser", user.getEmail(), e);
            return new UserResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    public List<String> fetchRoles() {
        String[] commaSeparatedroles = roles.split("\\s*,\\s*");
        return Arrays.stream(commaSeparatedroles).collect(Collectors.toList());
    }

    public UserResponseDTO updatePassword(PasswordDto passwordDto, String userName) {
        try {
            User user = userRepository.findByEmail(userName);

            boolean matches = bcryptEncoder.matches(passwordDto.getOldPassword(), user.getPassword());

            if (!matches) {
                return new UserResponseDTO(Status.OLD_PASSWORD_INVALID, null);
            }

            user.setPassword(bcryptEncoder.encode(passwordDto.getNewPassword()));
            user.setPendingPasswordUpdate(Boolean.FALSE);
            userRepository.save(user);

            setupMailbox(userName, passwordDto.getNewPassword(), false);

            return new UserResponseDTO(Status.SUCCESS, user);

        } catch (Exception e) {
            logError(SERVICE_NAME, "updatePassword", userName, e);
            return new UserResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    public UserResponseDTO updateUser(UpdateUserInputDto updateUserInputDto, String userId) {
        try {

            if(!isAdminUser()){
                return new UserResponseDTO(Status.FORBIDDEN);
            }

            Optional<User> userInstance = userRepository.findByUuidAndOrganisationId(userId, getTenantId());

            if(!userInstance.isPresent()){
                return new UserResponseDTO(Status.NOT_FOUND);
            }

            User user = userInstance.get();
            user.setFirstname(updateUserInputDto.getFirstname());
            user.setLastname(updateUserInputDto.getLastname());
            user.setRoles(updateUserInputDto.getRoles());
            userRepository.save(user);

            return new UserResponseDTO(Status.SUCCESS, user);

        } catch (Exception e) {
            logError(SERVICE_NAME, "updateUser", userId, e);
            return new UserResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    public UserResponseDTO deleteUser(String userId) {
        try {

            if(!isAdminUser()){
                return new UserResponseDTO(Status.FORBIDDEN);
            }

            Optional<User> userInstance = userRepository.findByUuidAndOrganisationId(userId, getTenantId());

            if(!userInstance.isPresent()){
                return new UserResponseDTO(Status.NOT_FOUND);
            }
            User user = userInstance.get();
            userRepository.delete(user);

            return new UserResponseDTO(Status.SUCCESS);

        } catch (Exception e) {
            logError(SERVICE_NAME, "updateUser", userId, e);
            return new UserResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    private Store getStore(String username, String password){
        try {
            //later switch to zoho
            return MailSessionHolder.getStore(username, password);
        }catch (MessagingException ex){
            return null;
        }
    }

    public UserResponseDTO migrateUser(MigrateUserInputDTO dto) {
        try {
            Store store = getStore(dto.getEmail(), dto.getPassword());
            if(store == null){
                return new UserResponseDTO(Status.FAILED_VALIDATION);
            }
            List<MigrationFolders> migrationFoldersList = getMigrationFolderList(store);

            Migration migration = Migration.initialise(getUserId(), dto.getEmail(), dto.getPassword(), migrationFoldersList);
            migrationRepository.save(migration);

            return new UserResponseDTO(Status.SUCCESS);
        } catch (Exception e){
            logError(SERVICE_NAME, "migrateUser", dto.getEmail(), e);
            return new UserResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    private List<MigrationFolders> getMigrationFolderList(Store store) throws MessagingException {
        List<MigrationFolders> migrationFoldersList = new ArrayList<>();

        migrationFoldersList.add(getFolder(store, "INBOX"));
        migrationFoldersList.add(getFolder(store, "Newsletters"));
        migrationFoldersList.add(getFolder(store, "Alerts"));
        migrationFoldersList.add(getFolder(store, "Sent"));
        return migrationFoldersList;
    }

    private MigrationFolders getFolder(Store store, String folderName) throws MessagingException {
        Folder inbox = store.getFolder(folderName);

        int totalMails = inbox != null ? inbox.getMessageCount() : 0;

        MigrationFolders migrationFolders = new MigrationFolders();
        migrationFolders.setName(folderName);
        migrationFolders.setTotalMails(totalMails);
        return migrationFolders;
    }

//    @Scheduled(fixedDelay = 5000)
//    public void handleMigration() throws MessagingException {
//        System.out.println("Starting migration ....");
//        Migration migration = migrationRepository.findFirstByStartedIsFalse();
//        if(migration == null){
//            System.out.println("I have entered into rest!");
//            return;
//        }
//
//        System.out.printf("starting migration for %s\n", migration.getUserName());
//
//
//        Folder userCurrentInbox = MailSessionHolder.getFromInboxFolder(migration.getUserId());
//        if(userCurrentInbox == null){
//            System.out.printf("user %s is not logged in at the moment... moving on \n", migration.getUserName());
//            return;
//        }
//
//        migration.setStarted(true);
//        migrationRepository.save(migration);
//
//        Store store = getStore(migration.getUserName(), migration.getPassword());
//        if(store == null){
//            System.out.println("weird: but we connected with these details before :( .. now ain't working no more");
//            return;
//        }
//
//        migrateFolder("INBOX", migration.getUserId(), store);
//        migrateFolder("Newsletters", migration.getUserId(), store);
//        migrateFolder("Alerts", migration.getUserId(), store);
//        migrateFolder("Sent", migration.getUserId(), store);
//
//        System.out.println("migration completed for user: "+ migration.getUserName());
//    }
//
//    private void migrateFolder(String folderName, String userId, Store store) throws MessagingException {
//        System.out.printf("Starting migration for folder %s\n", folderName);
//
//        Folder newFolder = MailSessionHolder.getFolderByName(folderName, userId);
//        if (newFolder == null){
//            System.out.printf("folder %s not found in store \n", folderName);
//            return;
//        }
//        Folder oldFolder = store.getFolder(folderName);
//        oldFolder.open(Folder.READ_ONLY);
//        System.out.println("total emails to migrate: "+oldFolder.getMessageCount());
//
//
//        newFolder.open(Folder.READ_WRITE);
//        newFolder.appendMessages(oldFolder.getMessages());
//
//        System.out.println("migration completed for folder: "+folderName);
//    }








    public UserResponseDTO addAlias(String email) {
        if(!isAdminUser()){
            return new UserResponseDTO(Status.FORBIDDEN);
        }


        return  new UserResponseDTO(Status.SUCCESS);

    }


    public StandardResponseDTO validateUserToken(String userEmail, String token){
        try {

            String tokenFromCache = sessionCacheManager.getTokenFromCache(userEmail);

            if(tokenFromCache == null || !tokenFromCache.equals(token)){
                return new StandardResponseDTO(Status.NOT_FOUND);
            }

            return new StandardResponseDTO(Status.SUCCESS);

        } catch (Exception e) {
            logError(SERVICE_NAME, "validateUserToken", userEmail, e);
            return new StandardResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    public BusinessNameResponseDTO registerBusinessName(BusinessNameInputDTO dto) {
        try{
            BusinessNameRegForm businessNameRegForm = new BusinessNameRegForm();
            setAndSaveBusinessDetails(dto, businessNameRegForm);
            return new BusinessNameResponseDTO(Status.SUCCESS, businessNameRegForm);
        } catch (Exception e) {
            logError(SERVICE_NAME, "registerBusiness", getUserEmail(), e);
            return new BusinessNameResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    private void setAndSaveBusinessDetails(BusinessNameInputDTO dto, BusinessNameRegForm businessNameRegForm) {
        String userId = getUserId();
        String userEmail = getUserEmail();
        businessNameRegForm.setRegType(setRegTypeParams(dto));
        businessNameRegForm.setBnDirectors(setBnDirectorsParams(dto));
        businessNameRegForm.setBnApplication(setBnApplicationParams(dto));
        businessNameRegForm.setUserEmail(userEmail);
        businessNameRegForm.setUserId(userId);
        businessNameRegForm.setTenantId(getTenantId());
        businessNameRegForm.setCreatedDate(new Date());

        businessNameRepository.save(businessNameRegForm);
    }

    private BNApplication setBnApplicationParams(BusinessNameInputDTO dto) {
        BNApplication bnApplication = new BNApplication();
        BNApplicationInputDTO applicationInput = dto.getBnApplicationInput();

        bnApplication.setRegNo(applicationInput.getRegNo());
        bnApplication.setBusinessName(applicationInput.getBusinessName());
        bnApplication.setGeneralBusinessNature(applicationInput.getGeneralBusinessNature());
        bnApplication.setPrincipalOfficeAddress(applicationInput.getPrincipalOfficeAddress());
        bnApplication.setBranchOfficeAddress(applicationInput.getBranchOfficeAddress());

        return bnApplication;
    }

    private List<BNDirectors> setBnDirectorsParams(BusinessNameInputDTO dto) {
        List<BNDirectors> bnDirectorsList = new ArrayList<BNDirectors>();
        List<BNDirectorsInputDTO> directorsInput = dto.getBnDirectorsInput();

        for (BNDirectorsInputDTO directorInput : directorsInput){
            BNDirectors bnDirectors = new BNDirectors();
            bnDirectors.setFullName(directorInput.getFullName());
            bnDirectors.setFormerName(directorInput.getFormerName());
            bnDirectors.setEmail(directorInput.getEmail());
            bnDirectors.setAge(directorInput.getAge());
            bnDirectors.setSex(directorInput.getSex());
            bnDirectors.setNationality(directorInput.getNationality());
            bnDirectors.setFormerNationality(directorInput.getFormerNationality());
            bnDirectors.setPhoneNumber(directorInput.getPhoneNumber());
            bnDirectors.setOccupation(directorInput.getOccupation());
            bnDirectors.setResidentialAddress(directorInput.getResidentialAddress());
            bnDirectors.setCity(directorInput.getCity());
            bnDirectors.setState(directorInput.getState());
            bnDirectors.setDate(new Date());

            bnDirectorsList.add(bnDirectors);
        }


        return bnDirectorsList;
    }

    private RegType setRegTypeParams(BusinessNameInputDTO dto) {
        RegType regType = new RegType();

        regType.setAimOfBusiness(dto.getRegTypeInput().getAimOfBusiness());
        regType.setBusinessNature(dto.getRegTypeInput().getBusinessNature());

        return regType;
    }


    public CompanyRegResponseDTO registerCompany(CompanyRegInputDTO dto) {
        try{
            CompanyRegForm companyRegForm = new CompanyRegForm();
            setAndSaveCompanyDetails(dto, companyRegForm);

            return new CompanyRegResponseDTO(Status.SUCCESS, companyRegForm);
        } catch (Exception e){
            logError(SERVICE_NAME, "registerCompany", getUserEmail(), e);
            return new CompanyRegResponseDTO(Status.INTERNAL_ERROR);

        }
    }

    private void setAndSaveCompanyDetails(CompanyRegInputDTO dto, CompanyRegForm companyRegForm) {
        String userId = getUserId();
        String userEmail = getUserEmail();
        companyRegForm.setTenantId(getTenantId());
        companyRegForm.setUserEmail(userEmail);
        companyRegForm.setUserId(userId);
        companyRegForm.setCreatedDate(new Date());
        companyRegForm.setRegType(setRegTypeParam(dto));
        companyRegForm.setCompanyApplication(setCompanyApplicationParams(dto));
        companyRegForm.setCompanyDirectors(setCompanyDirectorsParams(dto));
        companyRegForm.setCompanySecretaryFirm(setCompanySecFirmParams(dto));
        companyRegForm.setCompanySecretaryIndividual(setCompanySecIndividualParams(dto));

        companyRegRepository.save(companyRegForm);
    }

    private CompanySecretaryIndividual setCompanySecIndividualParams(CompanyRegInputDTO dto) {
        CompanySecretaryIndividual companySecretaryIndividual = new CompanySecretaryIndividual();
        CompanySecIndividualDTO companySecIndividualInput = dto.getCompanySecIndividualInput();

        companySecretaryIndividual.setAddress(companySecIndividualInput.getAddress());
        companySecretaryIndividual.setEmail(companySecIndividualInput.getEmail());
        companySecretaryIndividual.setIdNumber(companySecIndividualInput.getIdNumber());
        companySecretaryIndividual.setIdType(companySecIndividualInput.getIdType());
        companySecretaryIndividual.setName(companySecIndividualInput.getName());
        companySecretaryIndividual.setPhoneNumber(companySecIndividualInput.getPhoneNumber());

        return companySecretaryIndividual;
    }

    private CompanySecretaryFirm setCompanySecFirmParams(CompanyRegInputDTO dto) {
        CompanySecretaryFirm companySecretaryFirm = new CompanySecretaryFirm();
        CompanySecFirmInputDTO companySecFirmInput = dto.getCompanySecFirmInput();

        companySecretaryFirm.setAddress(companySecFirmInput.getAddress());
        companySecretaryFirm.setEmail(companySecFirmInput.getEmail());
        companySecretaryFirm.setBN_RC_Number(companySecFirmInput.getBN_RC_Number());
        companySecretaryFirm.setPhoneNumber(companySecFirmInput.getPhoneNumber());
        companySecretaryFirm.setName(companySecFirmInput.getName());

        return companySecretaryFirm;
    }

    private List<CompanyDirectors> setCompanyDirectorsParams(CompanyRegInputDTO dto) {
        List<CompanyDirectors> companyDirectorsList = new ArrayList<CompanyDirectors>();
        List<CompanyDirectorsInputDTO> companyDirectorsInputList = dto.getCompanyDirectorsInput();

        for(CompanyDirectorsInputDTO companyDirector : companyDirectorsInputList) {
            CompanyDirectors companyDirectors = new CompanyDirectors();
            companyDirectors.setAddress(companyDirector.getAddress());
            companyDirectors.setCity(companyDirector.getCity());
            companyDirectors.setCountry(companyDirector.getCountry());
            companyDirectors.setFullName(companyDirector.getFullName());
            companyDirectors.setGender(companyDirector.getGender());
            companyDirectors.setIdNumber(companyDirector.getIdNumber());
            companyDirectors.setIdType(companyDirector.getIdType());
            companyDirectors.setNationality(companyDirector.getNationality());
            companyDirectors.setPhoneNumber(companyDirector.getPhoneNumber());
            companyDirectors.setState(companyDirector.getState());

            companyDirectorsList.add(companyDirectors);
        }

        return companyDirectorsList;
    }

    private CompanyApplication setCompanyApplicationParams(CompanyRegInputDTO dto) {
        CompanyApplication companyApplication = new CompanyApplication();
        CompanyApplicationInputDTO companyApplicationInput = dto.getCompanyApplicationInput();

        companyApplication.setCompanyType(companyApplicationInput.getCompanyType());
        companyApplication.setDirectorName(companyApplicationInput.getDirectorName());
        companyApplication.setEmailAddress(companyApplicationInput.getEmailAddress());
        companyApplication.setHeadOfficeAddress(companyApplicationInput.getHeadOfficeAddress());
        companyApplication.setNumberOfShareUnits(companyApplicationInput.getNumberOfShareUnits());
        companyApplication.setPhoneNumber(companyApplicationInput.getPhoneNumber());
        companyApplication.setRegisteredOfficeAddress(companyApplicationInput.getRegisteredOfficeAddress());
        companyApplication.setShareCapitalInNumbers(companyApplicationInput.getShareCapitalInNumbers());
        companyApplication.setShareCapitalInWords(companyApplicationInput.getShareCapitalInWords());
        companyApplication.setShareUnitPrice(companyApplicationInput.getShareUnitPrice());

        return companyApplication;
    }

    private RegType setRegTypeParam(CompanyRegInputDTO dto) {
        RegType regType = new RegType();

        regType.setAimOfBusiness(dto.getRegTypeInput().getAimOfBusiness());
        regType.setBusinessNature(dto.getRegTypeInput().getBusinessNature());

        return regType;
    }

    public CheckNameResponseDTO checkNameAvailability(CheckNameInputDTO dto) {
        try{
            CheckName checkName = new CheckName();
            checkName.setName1(dto.getName1());
            checkName.setName2(dto.getName2());
            checkName.setCheckNameStatus(CheckNameStatus.PENDING);
            checkName.setCustomerEmail(getUserEmail());
            checkName.setCustomerFullName(getUsername());

            checkNameRepository.save(checkName);

            return new CheckNameResponseDTO(Status.SUCCESS, checkName);

        } catch (Exception e){
            logError(SERVICE_NAME, "checkNameAvailability", getUserEmail(), e);
            return new CheckNameResponseDTO(Status.INTERNAL_ERROR);
        }
    }


    public CheckNameResponseDTO fetchNameAvailabilityRecord(String checkNameId) {
        try{
            Optional<CheckName> checkName = checkNameRepository.findById(checkNameId);

            if(!checkName.isPresent()){
                return new CheckNameResponseDTO(Status.NOT_FOUND);
            }

            CheckName checkNameRecord = checkName.get();

            return new CheckNameResponseDTO(Status.SUCCESS, checkNameRecord);
        }catch (Exception e){
            logError(SERVICE_NAME, "fetchNameAvailabilityRecord", getUserEmail(), e);
            return new CheckNameResponseDTO(Status.INTERNAL_ERROR);
        }

    }


    public CheckNameListResponseDTO fetchNameAvailabilityCheckList() {
        try{
            Iterable<CheckName> checkNameList = checkNameRepository.findAll();
            return new CheckNameListResponseDTO(Status.SUCCESS, checkNameList);

        } catch (Exception e){
            logError(SERVICE_NAME, "fetchNameAvailabilityCheckList", getUserEmail(), e);
            return new CheckNameListResponseDTO(Status.INTERNAL_ERROR);
        }
    }


    public StandardResponseDTO nameCheckResponse(String checkNameId, NameCheckResponseInputDTO dto) {
        Optional<CheckName> checkName = checkNameRepository.findById(checkNameId);

        if(!checkName.isPresent()){
            return new StandardResponseDTO(Status.NOT_FOUND);
        }

        CheckName checkNameRecord = checkName.get();

        sortNameAvailability(dto, checkNameRecord);

        return new StandardResponseDTO(Status.SUCCESS);
    }

    private void sortNameAvailability(NameCheckResponseInputDTO dto, CheckName checkNameRecord) {
        String clientName = checkNameRecord.getCustomerFullName();

        String clientEmail = checkNameRecord.getCustomerEmail();

        String companyName1 = checkNameRecord.getName1();

        String companyName2 = checkNameRecord.getName2();

        boolean companyName1Status = dto.isName1Status();

        boolean companyName2Status = dto.isName2Status();

        filterResult(clientName, clientEmail, companyName1, companyName2, companyName1Status, companyName2Status);
    }

    private void filterResult(String clientName, String clientEmail, String companyName1, String companyName2, boolean companyName1Status, boolean companyName2Status) {
        if(companyName1Status & companyName2Status){
            String message = companyName1 + " and " + companyName2 + " are available.";
            sendResponseMail(clientEmail,clientName, message);
        }

        if(!companyName1Status & companyName2Status){
            String message = companyName1 + " is not available but " + companyName2 + " is available.";
            sendResponseMail(clientEmail,clientName, message);
        }

        if(companyName1Status & !companyName2Status){
            String message = companyName1 + " is available but " + companyName2 + " is not available.";
            sendResponseMail(clientEmail,clientName, message);
        }

        if(!companyName1Status & !companyName2Status){
            String message = companyName1  + " and  "  + companyName2 + " are both not available.";
            sendResponseMail(clientEmail,clientName, message);
        }
    }

    private void sendResponseMail(String email, String fullName , String message) {
        String from = "support@quabbly.com";
        String content = mailGenerator.checkNameMail(fullName, message);
        String subject = "Name Check Availability Response";
        List<String> recipient = new ArrayList<>();
        recipient.add(email);
        sendMail(from, content, subject, recipient);

    }

    private void sendMail(String from, String content, String subject, List<String> recipient) {
        EmailNotificationInputDTO confirmSignupMail = new EmailNotificationInputDTO(recipient, from, subject, content);

        CloseableHttpClient httpClient
                = HttpClients.custom().setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
        HttpComponentsClientHttpRequestFactory requestFactory
                = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);
        RestTemplate RestTemplate = new RestTemplate(requestFactory);

        HttpEntity requestPayload = new HttpEntity<>(confirmSignupMail);

        ResponseEntity<String> responseEntity =
                RestTemplate.postForEntity("https://quabbly-notification-api-dev.quabbly.com/v1/notification/send_email", requestPayload, String.class);
    }

}
