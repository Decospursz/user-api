package com.photizzo.quabbly.user.service;

import com.photizzo.quabbly.user.config.TokenProvider;
import com.photizzo.quabbly.user.dto.input.ApiFieldError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.validation.BindingResult;

import javax.mail.MessagingException;
import java.util.List;

import static java.util.stream.Collectors.toList;

abstract class AbstractService {

    @Value("${user.role.admin}")
    private String adminRole;

    void logError(String serviceName, String methodName, String user, Exception ex) {
        ex.printStackTrace(); //log in some Logging UI
    }

    void  logMessagingError(String serviceName, String methodName, String user, MessagingException ex){
        ex.printStackTrace();
    }

    @Autowired
    private TokenProvider tokenProvider;

    String getUserId() {
        return tokenProvider.getUserId();
    }

    String getTenantId() {
        return tokenProvider.getTenantId();
    }

    String getUserEmail() {
        return tokenProvider.getUserEmail();
    }

    String getUsername() {
        return tokenProvider.getUserName();
    }

    protected boolean isAdminUser(){
        return tokenProvider.getRoles() != null && tokenProvider.getRoles().contains(adminRole);
    }

    List<ApiFieldError> getValidationErrors(BindingResult result) {
        return result.getFieldErrors()
                .stream()
                .map(fieldError -> new ApiFieldError(
                        fieldError.getField(),
                        fieldError.getDefaultMessage(),
                        fieldError.getRejectedValue())
                )
                .collect(toList());
    }

}
