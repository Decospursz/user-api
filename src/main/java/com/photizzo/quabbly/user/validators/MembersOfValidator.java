package com.photizzo.quabbly.user.validators;

import com.photizzo.quabbly.user.validators.interfaces.MembersOf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class MembersOfValidator implements ConstraintValidator<MembersOf, Object> {
    private Class className;
    private String columnName;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void initialize(final MembersOf annotation) {
        className = annotation.className();
        columnName = annotation.columnName();
    }

    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {

        if (value == null) {
            return true;
        }

        String[] ids = (String[]) value;
        for (String id : ids) {
            Query query = new Query();
            query.addCriteria(Criteria.where(columnName).is(id));

            Object o = mongoTemplate.findOne(query, className);

            if (o == null) {
                return false;
            }
        }

        return true;
    }

}