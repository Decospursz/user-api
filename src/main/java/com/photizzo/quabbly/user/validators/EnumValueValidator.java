package com.photizzo.quabbly.user.validators;

import com.photizzo.quabbly.user.validators.interfaces.EnumValue;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class EnumValueValidator implements ConstraintValidator<EnumValue, Object> {
    private Object[] enumValues;

    @Override
    public void initialize(final EnumValue annotation) {
        enumValues = annotation.enumClass().getEnumConstants();
    }

    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {
        if (value != null) {
            String contextValue = value.toString();

            for (Object enumValue : enumValues) {
                if (enumValue.toString().equalsIgnoreCase(contextValue)) {
                    return true;
                }
            }
        }

        return false;
    }

}