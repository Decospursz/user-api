package com.photizzo.quabbly.user.validators.interfaces;

import com.photizzo.quabbly.user.validators.MembersOfValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = {MembersOfValidator.class})
@Retention(RetentionPolicy.RUNTIME)

@Target({
        ElementType.ANNOTATION_TYPE,
        ElementType.FIELD
})

public @interface MembersOf {
    String message() default "";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String columnName();

    Class<?> className();

}
