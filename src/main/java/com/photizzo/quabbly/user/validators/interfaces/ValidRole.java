package com.photizzo.quabbly.user.validators.interfaces;

import com.photizzo.quabbly.user.validators.ValidRoleValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = {ValidRoleValidator.class})
@Retention(RetentionPolicy.RUNTIME)

@Target({
        ElementType.ANNOTATION_TYPE,
        ElementType.CONSTRUCTOR,
        ElementType.FIELD,
        ElementType.METHOD,
        ElementType.PARAMETER
})

public @interface ValidRole {
    String message() default "{validation.roles.invalid}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
