package com.photizzo.quabbly.user.dto.input;

import com.photizzo.quabbly.user.model.User;
import com.photizzo.quabbly.user.validators.interfaces.UniqueField;

import javax.validation.GroupSequence;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@GroupSequence({
        RegisterUserInputDto.First.class,
        RegisterUserInputDto.Second.class,
        RegisterUserInputDto.Third.class,
        RegisterUserInputDto.class
})
public class RegisterUserInputDto {

    @NotNull(message = "{user.firstname.notEmpty}", groups = First.class)
    @Size(min = 3, max = 50, message = "{user.firstname.sizeError}", groups = Second.class)
    private String firstname;
    @NotNull(message = "{user.lastname.notEmpty}", groups = First.class)
    @Size(min = 3, max = 50, message = "{user.lastname.sizeError}", groups = Second.class)
    private String lastname;
    @Email(message = "{user.email.validEmail}", groups = Second.class)
    @NotNull(message = "{user.email.notEmpty}", groups = First.class)
    @UniqueField(message = "{user.email.unique}", columnName = "email", className = User.class, groups = Third.class)
    private String email;
    @NotNull(message = "{user.password.notEmpty}", groups = First.class)
    @Size(min = 5, message = "{user.password.sizeError}", groups = Second.class)
    private String password;


    public RegisterUserInputDto() {
    }

    public RegisterUserInputDto(@NotNull(message = "{user.firstname.notEmpty}", groups = First.class) @Size(min = 3, max = 50, message = "{user.firstname.sizeError}", groups = Second.class) String firstname, @NotNull(message = "{user.lastname.notEmpty}", groups = First.class) @Size(min = 3, max = 50, message = "{user.lastname.sizeError}", groups = Second.class) String lastname, @Email(message = "{user.email.validEmail}", groups = Second.class) @NotNull(message = "{user.email.notEmpty}", groups = First.class) String email, @NotNull(message = "{user.password.notEmpty}", groups = First.class) @Size(min = 5, message = "{user.password.sizeError}", groups = Second.class) String password) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    interface First {
    }

    interface Second {
    }

    interface Third {
    }

}
