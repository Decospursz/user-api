package com.photizzo.quabbly.user.dto.input;

import com.photizzo.quabbly.user.validators.interfaces.ValidRole;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;


@GroupSequence({
        UpdateUserInputDto.First.class,
        UpdateUserInputDto.Second.class,
        UpdateUserInputDto.class
})
public class UpdateUserInputDto {

    @NotNull(message = "{user.firstname.notEmpty}", groups = First.class)
    @Size(min = 3, max = 50, message = "{user.firstname.sizeError}", groups = Second.class)
    private String firstname;
    @NotNull(message = "{user.lastname.notEmpty}", groups = First.class)
    @Size(min = 3, max = 50, message = "{user.lastname.sizeError}", groups = Second.class)
    private String lastname;

    @NotNull(message = "{user.roles.notEmpty}", groups = First.class)
    @ValidRole(groups = Second.class)
    private List<String> roles;





    public UpdateUserInputDto() {
    }


    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    interface First {
    }

    interface Second {
    }


}
