package com.photizzo.quabbly.user.dto.output.bizReg;

import com.photizzo.quabbly.user.dto.enums.Status;
import com.photizzo.quabbly.user.dto.output.StandardResponseDTO;
import com.photizzo.quabbly.user.model.bizReg.CheckName;


public class CheckNameListResponseDTO  extends StandardResponseDTO {
    private Iterable<CheckName> checkNameList;

    public CheckNameListResponseDTO() {
    }

    public CheckNameListResponseDTO(Status status) {
        super(status);
    }

    public CheckNameListResponseDTO(Status status, Iterable<CheckName> checkNameList) {
        super(status);
        this.checkNameList = checkNameList;
    }

    public Iterable<CheckName> getCheckNameList() {
        return checkNameList;
    }

    public void setCheckNameList(Iterable<CheckName> checkNameList) {
        this.checkNameList = checkNameList;
    }
}
