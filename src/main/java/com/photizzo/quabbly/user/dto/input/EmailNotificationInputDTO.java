package com.photizzo.quabbly.user.dto.input;

import java.util.List;

public class EmailNotificationInputDTO {
    private List<String> recipient;
    private String from;
    private String subject;
    private String content;


    public EmailNotificationInputDTO(List<String> recipient, String from, String subject, String content) {
        this.recipient = recipient;
        this.from = from;
        this.subject = subject;
        this.content = content;
    }

    public List<String> getRecipient() {
        return recipient;
    }

    public void setRecipient(List<String> recipient) {
        this.recipient = recipient;
    }

    public String getfrom() {
        return from;
    }

    public void setfrom(String from) {
        this.from = from;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
