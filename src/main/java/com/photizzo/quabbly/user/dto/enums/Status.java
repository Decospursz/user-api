package com.photizzo.quabbly.user.dto.enums;

public enum Status {
    SUCCESS, INTERNAL_ERROR, FAILED_VALIDATION, OLD_PASSWORD_INVALID, NOT_FOUND, FORBIDDEN

}
