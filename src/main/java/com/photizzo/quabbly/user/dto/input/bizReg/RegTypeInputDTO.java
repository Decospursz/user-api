package com.photizzo.quabbly.user.dto.input.bizReg;

import java.util.List;

public class RegTypeInputDTO {
    private List<String> aimOfBusiness;

    private String businessNature;

    public List<String> getAimOfBusiness() {
        return aimOfBusiness;
    }

    public void setAimOfBusiness(List<String> aimOfBusiness) {
        this.aimOfBusiness = aimOfBusiness;
    }

    public String getBusinessNature() {
        return businessNature;
    }

    public void setBusinessNature(String businessNature) {
        this.businessNature = businessNature;
    }
}
