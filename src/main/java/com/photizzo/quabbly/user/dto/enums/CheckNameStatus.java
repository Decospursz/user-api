package com.photizzo.quabbly.user.dto.enums;

public enum CheckNameStatus {
    PENDING, DONE
}
