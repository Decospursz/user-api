package com.photizzo.quabbly.user.dto.input;

import javax.validation.constraints.NotNull;
import java.util.List;

public class MarkAsReadInputDto {

    @NotNull(message = "valid mail ids are required")
    private List<String> ids;

    @NotNull(message = "folder name is required")
    private String folderName;

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public List<String> getIds() {
        return ids;
    }

    public void setIds(List<String> ids) {
        this.ids = ids;
    }
}
