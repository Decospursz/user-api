package com.photizzo.quabbly.user.dto.input.bizReg;

public class NameCheckResponseInputDTO {

    private boolean name1Status;

    private boolean name2Status;

    public boolean isName1Status() {
        return name1Status;
    }

    public void setName1Status(boolean name1Status) {
        this.name1Status = name1Status;
    }

    public boolean isName2Status() {
        return name2Status;
    }

    public void setName2Status(boolean name2Status) {
        this.name2Status = name2Status;
    }
}
