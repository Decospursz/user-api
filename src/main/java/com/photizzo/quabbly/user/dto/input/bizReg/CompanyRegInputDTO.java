package com.photizzo.quabbly.user.dto.input.bizReg;

import com.photizzo.quabbly.user.model.bizReg.*;

import java.util.List;

public class CompanyRegInputDTO {

    private RegTypeInputDTO regTypeInput;

    private CompanyApplicationInputDTO companyApplicationInput;

    private List<CompanyDirectorsInputDTO> companyDirectorsInput;

    private CompanySecIndividualDTO companySecIndividualInput;

    private CompanySecFirmInputDTO companySecFirmInput;

    public RegTypeInputDTO getRegTypeInput() {
        return regTypeInput;
    }

    public void setRegTypeInput(RegTypeInputDTO regTypeInput) {
        this.regTypeInput = regTypeInput;
    }

    public CompanyApplicationInputDTO getCompanyApplicationInput() {
        return companyApplicationInput;
    }

    public void setCompanyApplicationInput(CompanyApplicationInputDTO companyApplicationInput) {
        this.companyApplicationInput = companyApplicationInput;
    }

    public List<CompanyDirectorsInputDTO> getCompanyDirectorsInput() {
        return companyDirectorsInput;
    }

    public void setCompanyDirectorsInput(List<CompanyDirectorsInputDTO> companyDirectorsInput) {
        this.companyDirectorsInput = companyDirectorsInput;
    }

    public CompanySecIndividualDTO getCompanySecIndividualInput() {
        return companySecIndividualInput;
    }

    public void setCompanySecIndividualInput(CompanySecIndividualDTO companySecIndividualInput) {
        this.companySecIndividualInput = companySecIndividualInput;
    }

    public CompanySecFirmInputDTO getCompanySecFirmInput() {
        return companySecFirmInput;
    }

    public void setCompanySecFirmInput(CompanySecFirmInputDTO companySecFirmInput) {
        this.companySecFirmInput = companySecFirmInput;
    }
}
