package com.photizzo.quabbly.user.dto.output.bizReg;

import com.photizzo.quabbly.user.dto.enums.Status;
import com.photizzo.quabbly.user.dto.input.ApiFieldError;
import com.photizzo.quabbly.user.dto.output.StandardResponseDTO;
import com.photizzo.quabbly.user.model.bizReg.BusinessNameRegForm;

import java.util.List;

public class BusinessNameResponseDTO extends StandardResponseDTO {

    private BusinessNameRegForm businessNameRegForm;



    public BusinessNameResponseDTO(Status status) {
        super(status);
    }

    public BusinessNameResponseDTO(Status status, BusinessNameRegForm businessNameRegForm) {
        super(status);
        this.businessNameRegForm = businessNameRegForm;
    }

    public BusinessNameResponseDTO(Status status, List<ApiFieldError> errors, BusinessNameRegForm businessNameRegForm) {
        super(status, errors);
        this.businessNameRegForm = businessNameRegForm;
    }



    public BusinessNameRegForm getBusinessNameRegForm() {
        return businessNameRegForm;
    }

    public void setBusinessNameRegForm(BusinessNameRegForm businessNameRegForm) {
        this.businessNameRegForm = businessNameRegForm;
    }
}
