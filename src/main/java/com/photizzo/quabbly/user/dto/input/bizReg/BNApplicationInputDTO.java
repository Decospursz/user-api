package com.photizzo.quabbly.user.dto.input.bizReg;

public class BNApplicationInputDTO {
    private String regNo;

    private String businessName;

    private String generalBusinessNature;

    private String principalOfficeAddress;

    private String branchOfficeAddress;

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getGeneralBusinessNature() {
        return generalBusinessNature;
    }

    public void setGeneralBusinessNature(String generalBusinessNature) {
        this.generalBusinessNature = generalBusinessNature;
    }

    public String getPrincipalOfficeAddress() {
        return principalOfficeAddress;
    }

    public void setPrincipalOfficeAddress(String principalOfficeAddress) {
        this.principalOfficeAddress = principalOfficeAddress;
    }

    public String getBranchOfficeAddress() {
        return branchOfficeAddress;
    }

    public void setBranchOfficeAddress(String branchOfficeAddress) {
        this.branchOfficeAddress = branchOfficeAddress;
    }
}
