package com.photizzo.quabbly.user.dto.output;

import com.photizzo.quabbly.user.dto.enums.Status;
import com.photizzo.quabbly.user.mailbox.MessagePartAttachment;

public class DownloadResponseDTO extends StandardResponseDTO {

    private MessagePartAttachment attachment;
    private String error;

    public DownloadResponseDTO() {

    }

    public DownloadResponseDTO(Status status) {
        super(status);
    }

    public DownloadResponseDTO(Status status, MessagePartAttachment attachment) {
        super(status);
        this.attachment = attachment;
    }

    public DownloadResponseDTO(Status status, String error) {
        super(status);
        this.error = error;
    }


    public MessagePartAttachment getAttachment() {
        return attachment;
    }

    public void setAttachment(MessagePartAttachment attachment) {
        this.attachment = attachment;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
