package com.photizzo.quabbly.user.dto.output;

import com.photizzo.quabbly.user.dto.enums.Status;
import com.photizzo.quabbly.user.mailbox.QuabblyMessageHolder;

public class SentMailResponseDTO extends StandardResponseDTO {

    private QuabblyMessageHolder summary;

    public SentMailResponseDTO() {

    }

    public SentMailResponseDTO(Status status) {
        super(status);
    }

    public SentMailResponseDTO(Status status, QuabblyMessageHolder summary) {
        super(status);
        this.summary = summary;
    }


    public QuabblyMessageHolder getSummary() {
        return summary;
    }

    public void setSummary(QuabblyMessageHolder summary) {
        this.summary = summary;
    }
}
