package com.photizzo.quabbly.user.dto.output.bizReg;

import com.photizzo.quabbly.user.dto.enums.Status;
import com.photizzo.quabbly.user.dto.output.StandardResponseDTO;
import com.photizzo.quabbly.user.model.bizReg.CheckName;

public class CheckNameResponseDTO extends StandardResponseDTO {
    private CheckName checkName;

    public CheckNameResponseDTO(Status status) {
        super(status);
    }

    public CheckNameResponseDTO(CheckName checkName) {
        this.checkName = checkName;
    }

    public CheckNameResponseDTO(Status status, CheckName checkName) {
        super(status);
        this.checkName = checkName;
    }

    public CheckName getCheckName() {
        return checkName;
    }

    public void setCheckName(CheckName checkName) {
        this.checkName = checkName;
    }
}
