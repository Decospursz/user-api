package com.photizzo.quabbly.user.dto.output.bizReg;


import com.photizzo.quabbly.user.dto.enums.Status;
import com.photizzo.quabbly.user.dto.input.ApiFieldError;
import com.photizzo.quabbly.user.dto.output.StandardResponseDTO;
import com.photizzo.quabbly.user.model.bizReg.CompanyRegForm;

import java.util.List;

public class CompanyRegResponseDTO extends StandardResponseDTO {

    private CompanyRegForm companyRegForm;

    public CompanyRegResponseDTO() {
    }

    public CompanyRegResponseDTO(Status status) {
        super(status);
    }

    public CompanyRegResponseDTO(Status status, CompanyRegForm companyRegForm) {
        super(status);
        this.companyRegForm = companyRegForm;
    }

    public CompanyRegResponseDTO(Status status, List<ApiFieldError> errors, CompanyRegForm companyRegForm) {
        super(status, errors);
        this.companyRegForm = companyRegForm;
    }

    public CompanyRegForm getCompanyRegForm() {
        return companyRegForm;
    }

    public void setCompanyRegForm(CompanyRegForm companyRegForm) {
        this.companyRegForm = companyRegForm;
    }
}
