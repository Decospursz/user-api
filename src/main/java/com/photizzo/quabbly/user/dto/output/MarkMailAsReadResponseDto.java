package com.photizzo.quabbly.user.dto.output;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.photizzo.quabbly.user.dto.enums.Status;

import java.util.List;


public class MarkMailAsReadResponseDto extends StandardResponseDTO {

    @JsonProperty
    private List<String> marked;

    public MarkMailAsReadResponseDto() {
    }

    public MarkMailAsReadResponseDto(Status status) {
        super(status);
    }

    public MarkMailAsReadResponseDto(Status status, List<String> marked) {
        super(status);
        this.marked = marked;
    }

    public List<String> getMarked() {
        return marked;
    }

    public void setMarked(List<String> marked) {
        this.marked = marked;
    }
}
