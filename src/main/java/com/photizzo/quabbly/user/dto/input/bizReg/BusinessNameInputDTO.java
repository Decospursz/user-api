package com.photizzo.quabbly.user.dto.input.bizReg;


import java.util.List;

public class BusinessNameInputDTO {
  private RegTypeInputDTO regTypeInput;

  private BNApplicationInputDTO bnApplicationInput;

  private List<BNDirectorsInputDTO> bnDirectorsInput;

    public RegTypeInputDTO getRegTypeInput() {
        return regTypeInput;
    }

    public void setRegTypeInput(RegTypeInputDTO regTypeInput) {
        this.regTypeInput = regTypeInput;
    }

    public BNApplicationInputDTO getBnApplicationInput() {
        return bnApplicationInput;
    }

    public void setBnApplicationInput(BNApplicationInputDTO bnApplicationInput) {
        this.bnApplicationInput = bnApplicationInput;
    }

    public List<BNDirectorsInputDTO> getBnDirectorsInput() {
        return bnDirectorsInput;
    }

    public void setBnDirectorsInput(List<BNDirectorsInputDTO> bnDirectorsInput) {
        this.bnDirectorsInput = bnDirectorsInput;
    }
}
