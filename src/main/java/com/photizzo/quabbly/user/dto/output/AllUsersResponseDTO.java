package com.photizzo.quabbly.user.dto.output;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.photizzo.quabbly.user.dto.enums.Status;
import com.photizzo.quabbly.user.model.User;

import java.io.Serializable;
import java.util.List;


public class AllUsersResponseDTO extends StandardResponseDTO implements Serializable {

    @JsonProperty
    private List<User> data;

    public AllUsersResponseDTO() {
    }

    public AllUsersResponseDTO(Status status) {
        super(status);
    }

    public AllUsersResponseDTO(Status status, List<User> data) {
        super(status);
        this.data = data;
    }


    public List<User> getData() {
        return data;
    }

    public void setData(List<User> data) {
        this.data = data;
    }
}
