package com.photizzo.quabbly.user.dto.input;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SendEmailInputDTO {

    @NotNull(message = "subject is required")
    private String subject;
    @NotNull(message = "at least one recipient is required")
    private String recipients;
    private String cc;
    private String bcc;
    private String content;
    private MultipartFile[] files;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getRecipients() {
        return recipients != null ? recipients : "";
    }

    public void setRecipients(String recipients) {
        this.recipients = recipients;
    }

    public String getCc() {
        return cc != null ? cc : "";
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getBcc() {
        return bcc != null ? bcc : "";
    }

    public void setBcc(String bcc) {
        this.bcc = bcc;
    }

    public MultipartFile[] getFiles() {
        return files != null ? files : new MultipartFile[0];
    }

    public void setFiles(MultipartFile[] files) {
        this.files = files;
    }

    public String getContent() {
        return content != null ? content : "";
    }

    public void setContent(String content) {
        this.content = content;
    }
}
