package com.photizzo.quabbly.user.dto.output;

import com.photizzo.quabbly.user.dto.enums.Status;
import com.photizzo.quabbly.user.model.User;

public class UserResponseDTO extends StandardResponseDTO {

    private User user;

    private String errorMessage;

    public UserResponseDTO() {
    }

    public UserResponseDTO(Status status, User user) {
        super(status);
        this.user = user;
    }

    public UserResponseDTO(Status status) {
        this(status, null);
    }



    public UserResponseDTO(Status status,User user, String errorMessage) {
        super(status);
        this.user = user;
        this.errorMessage = errorMessage;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
