package com.photizzo.quabbly.user.dto.enums;

public enum BooleanTypes {
    TRUE, FALSE
}
