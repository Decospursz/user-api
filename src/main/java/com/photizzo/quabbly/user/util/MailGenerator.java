package com.photizzo.quabbly.user.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;


import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Component
public class MailGenerator {
    @Autowired
    private TemplateEngine templateEngine;

    public String generateMailHtml(String text)
    {
        Map<String, Object> variables = new HashMap<>();
        variables.put("username", text);

        final String templateFileName = "mail"; //Name of the template file without extension

        return this.templateEngine.process(templateFileName, new Context(Locale.getDefault(), variables));


    }

    public String checkNameMail(String fullName, String mailContent)
    {
        Map<String, Object> variables = new HashMap<>();
        variables.put("fullName", fullName);
        variables.put("message", mailContent);

        final String templateFileName = "checknameresponsemail";  //Name of the template file without extension

        return this.templateEngine.process(templateFileName, new Context(Locale.getDefault(), variables));

    }
}
