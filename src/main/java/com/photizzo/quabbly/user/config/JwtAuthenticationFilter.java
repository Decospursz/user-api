package com.photizzo.quabbly.user.config;

import com.photizzo.quabbly.user.repository.redis.SessionCacheManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.photizzo.quabbly.user.dto.input.Constants.HEADER_STRING;
import static com.photizzo.quabbly.user.dto.input.Constants.TOKEN_PREFIX;


public class JwtAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private TokenProvider tokenProvider;

    @Autowired
    private SessionCacheManager sessionCacheManager;

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        String header = req.getHeader(HEADER_STRING);
        String username = null;
        String authToken = null;
        if (header != null && header.startsWith(TOKEN_PREFIX)) {
            authToken = header.replace(TOKEN_PREFIX, "");
            username = tokenProvider.getUsernameFromToken(authToken);
        } else if (req.getParameter("token") != null) {
            authToken = req.getParameter("token");
            username = tokenProvider.getUsernameFromToken(authToken);
        } else {
            logger.warn("couldn't find bearer string, will ignore the header");
        }

        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {

            UserDetails userDetails = userDetailsService.loadUserByUsername(username);

            if (userExistsInSessionCache(username, authToken)) {

                setSecurityContext(req, authToken, userDetails);

                updateUserInSessionCache(username, authToken);

                setUserDetailsInTokenProvider(authToken);
            } else {
                logger.warn("Token failed validation");
            }

        }

        chain.doFilter(req, res);
    }

    private void setUserDetailsInTokenProvider(String authToken) {
        tokenProvider.setDetails(authToken);
    }

    private void updateUserInSessionCache(String username, String authToken) {
        sessionCacheManager.storeTokenInCache(username, authToken);
    }

    private void setSecurityContext(HttpServletRequest req, String authToken, UserDetails userDetails) {
        UsernamePasswordAuthenticationToken authentication = tokenProvider.getAuthentication(authToken, SecurityContextHolder.getContext().getAuthentication(), userDetails);
        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(req));

        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    private boolean userExistsInSessionCache(String username, String authToken) {
        String tokenSavedInCache = sessionCacheManager.getTokenFromCache(username);
        return tokenSavedInCache != null && tokenSavedInCache.equals(authToken);
    }
}