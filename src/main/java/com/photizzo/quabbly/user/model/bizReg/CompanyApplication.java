package com.photizzo.quabbly.user.model.bizReg;

import org.springframework.data.annotation.Id;

public class CompanyApplication {


   private String companyType;

   private String headOfficeAddress;

   private String registeredOfficeAddress;

   private String emailAddress;

   private String shareCapitalInWords;

   private int shareCapitalInNumbers;

   private int numberOfShareUnits;

   private int shareUnitPrice;

   private String directorName;

   private String phoneNumber;


    public CompanyApplication() {
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public String getHeadOfficeAddress() {
        return headOfficeAddress;
    }

    public void setHeadOfficeAddress(String headOfficeAddress) {
        this.headOfficeAddress = headOfficeAddress;
    }

    public String getRegisteredOfficeAddress() {
        return registeredOfficeAddress;
    }

    public void setRegisteredOfficeAddress(String registeredOfficeAddress) {
        this.registeredOfficeAddress = registeredOfficeAddress;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getShareCapitalInWords() {
        return shareCapitalInWords;
    }

    public void setShareCapitalInWords(String shareCapitalInWords) {
        this.shareCapitalInWords = shareCapitalInWords;
    }

    public int getShareCapitalInNumbers() {
        return shareCapitalInNumbers;
    }

    public void setShareCapitalInNumbers(int shareCapitalInNumbers) {
        this.shareCapitalInNumbers = shareCapitalInNumbers;
    }

    public int getNumberOfShareUnits() {
        return numberOfShareUnits;
    }

    public void setNumberOfShareUnits(int numberOfShareUnits) {
        this.numberOfShareUnits = numberOfShareUnits;
    }

    public int getShareUnitPrice() {
        return shareUnitPrice;
    }

    public void setShareUnitPrice(int shareUnitPrice) {
        this.shareUnitPrice = shareUnitPrice;
    }

    public String getDirectorName() {
        return directorName;
    }

    public void setDirectorName(String directorName) {
        this.directorName = directorName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

}
