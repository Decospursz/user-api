package com.photizzo.quabbly.user.model.migration;

import com.bol.secure.Encrypted;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.photizzo.quabbly.user.validators.interfaces.EqualsTo;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
public class Migration {
    @Id
    private String id;

    private Source source;
    private String userId;
    private String userName;

    @JsonIgnore
    @Encrypted
    private String password;

    private List<MigrationFolders> folders;

    private boolean started = false;

    public Migration() {
    }

    public static Migration initialise(String userId,  String userName, String password, List<MigrationFolders> folders){
        return new Migration(userId, userName, password, folders, false);
    }

    public Migration(String userId, String userName, String password, List<MigrationFolders> folders, boolean started) {
        this.userId = userId;
        this.userName = userName;
        this.password = password;
        this.folders = folders;
        this.started = started;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<MigrationFolders> getFolders() {
        return folders;
    }

    public void setFolders(List<MigrationFolders> folders) {
        this.folders = folders;
    }

    public boolean isStarted() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }
}
