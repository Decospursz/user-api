package com.photizzo.quabbly.user.model.migration;

public class MigrationFolders {
    private String name;

    private int totalMigratedMails;

    private int totalMails;

    private boolean isMigrated;

    public MigrationFolders() {

    }

    public MigrationFolders(String name, int totalMigratedMails, int totalMails, boolean isMigrated) {
        this.name = name;
        this.totalMigratedMails = totalMigratedMails;
        this.totalMails = totalMails;
        this.isMigrated = isMigrated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalMigratedMails() {
        return totalMigratedMails;
    }

    public void setTotalMigratedMails(int totalMigratedMails) {
        this.totalMigratedMails = totalMigratedMails;
    }

    public int getTotalMails() {
        return totalMails;
    }

    public void setTotalMails(int totalMails) {
        this.totalMails = totalMails;
    }

    public boolean isMigrated() {
        return isMigrated;
    }

    public void setMigrated(boolean migrated) {
        isMigrated = migrated;
    }
}
