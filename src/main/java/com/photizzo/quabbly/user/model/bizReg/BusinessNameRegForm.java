package com.photizzo.quabbly.user.model.bizReg;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;


@Document
public class BusinessNameRegForm {
    @Id
    private String id;

    private String userId;

    private String tenantId;

    private String userEmail;

    private RegType regType;

    private BNApplication bnApplication;

    private List<BNDirectors> bnDirectors;

    private Date createdDate;

    public BusinessNameRegForm() {

    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public RegType getRegType() {
        return regType;
    }

    public void setRegType(RegType regType) {
        this.regType = regType;
    }

    public BNApplication getBnApplication() {
        return bnApplication;
    }

    public void setBnApplication(BNApplication bnApplication) {
        this.bnApplication = bnApplication;
    }

    public List<BNDirectors> getBnDirectors() {
        return bnDirectors;
    }

    public void setBnDirectors(List<BNDirectors> bnDirectors) {
        this.bnDirectors = bnDirectors;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
