package com.photizzo.quabbly.user.model.bizReg;

import org.springframework.data.annotation.Id;

public class CompanySecretaryFirm {

    private String Name;

    private String Address;

    private String BN_RC_Number;

    private String email;

    private int phoneNumber;

    private String Signature;

    public CompanySecretaryFirm() {
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getBN_RC_Number() {
        return BN_RC_Number;
    }

    public void setBN_RC_Number(String BN_RC_Number) {
        this.BN_RC_Number = BN_RC_Number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSignature() {
        return Signature;
    }

    public void setSignature(String signature) {
        Signature = signature;
    }
}
