package com.photizzo.quabbly.user.model.bizReg;

import com.photizzo.quabbly.user.dto.enums.CheckNameStatus;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class CheckName {
    @Id
    private String id;

    private String customerFullName;

    private String customerEmail;

    private String name1;

    private String name2;

    private boolean name1status;

    private  boolean name2status;

    private CheckNameStatus checkNameStatus;



    public CheckName() {
    }


    public CheckName(String customerFullName, String customerEmail, String name1, String name2, boolean name1status, boolean name2status) {
        this.customerFullName = customerFullName;
        this.customerEmail = customerEmail;
        this.name1 = name1;
        this.name2 = name2;
        this.name1status = name1status;
        this.name2status = name2status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomerFullName() {
        return customerFullName;
    }

    public void setCustomerFullName(String customerFullName) {
        this.customerFullName = customerFullName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public boolean isName1status() {
        return name1status;
    }

    public void setName1status(boolean name1status) {
        this.name1status = name1status;
    }

    public boolean isName2status() {
        return name2status;
    }

    public void setName2status(boolean name2status) {
        this.name2status = name2status;
    }

    public CheckNameStatus getCheckNameStatus() {
        return checkNameStatus;
    }

    public void setCheckNameStatus(CheckNameStatus checkNameStatus) {
        this.checkNameStatus = checkNameStatus;
    }
}
