package com.photizzo.quabbly.user.model.bizReg;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document
public class CompanyRegForm {
   @Id
    private String id;

   private RegType regType;

   private CompanyApplication companyApplication;

   private List<CompanyDirectors> companyDirectors;

   private CompanySecretaryFirm companySecretaryFirm;

   private CompanySecretaryIndividual companySecretaryIndividual;

    private String userId;

    private String tenantId;

    private String userEmail;

    private Date createdDate;

    public CompanyRegForm() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public RegType getRegType() {
        return regType;
    }

    public void setRegType(RegType regType) {
        this.regType = regType;
    }

    public CompanyApplication getCompanyApplication() {
        return companyApplication;
    }

    public void setCompanyApplication(CompanyApplication companyApplication) {
        this.companyApplication = companyApplication;
    }

    public List<CompanyDirectors> getCompanyDirectors() {
        return companyDirectors;
    }

    public void setCompanyDirectors(List<CompanyDirectors> companyDirectors) {
        this.companyDirectors = companyDirectors;
    }

    public CompanySecretaryFirm getCompanySecretaryFirm() {
        return companySecretaryFirm;
    }

    public void setCompanySecretaryFirm(CompanySecretaryFirm companySecretaryFirm) {
        this.companySecretaryFirm = companySecretaryFirm;
    }

    public CompanySecretaryIndividual getCompanySecretaryIndividual() {
        return companySecretaryIndividual;
    }

    public void setCompanySecretaryIndividual(CompanySecretaryIndividual companySecretaryIndividual) {
        this.companySecretaryIndividual = companySecretaryIndividual;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
