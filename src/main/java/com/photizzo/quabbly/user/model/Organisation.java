package com.photizzo.quabbly.user.model;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class Organisation {

    @Id
    private String id;

    @Indexed(unique = true)
    private String uuid;

    @Indexed
    private String name;

    private String phone;

    private int numberOfExistingUsers;

    private int numberOfFreeUsers;

    private int pricePerUserPerMonth;

    private int pricePerUserPerYear;

    private int billingCycleDays;

    private int monthlyBill;

    @CreatedDate
    private Date createdDate;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public int getNumberOfFreeUsers() {
        return numberOfFreeUsers;
    }

    public void setNumberOfFreeUsers(int numberOfFreeUsers) {
        this.numberOfFreeUsers = numberOfFreeUsers;
    }

    public int getPricePerUserPerMonth() {
        return pricePerUserPerMonth;
    }

    public void setPricePerUserPerMonth(int pricePerUserPerMonth) {
        this.pricePerUserPerMonth = pricePerUserPerMonth;
    }

    public int getPricePerUserPerYear() {
        return pricePerUserPerYear;
    }

    public void setPricePerUserPerYear(int pricePerUserPerYear) {
        this.pricePerUserPerYear = pricePerUserPerYear;
    }

    public int getBillingCycleDays() {
        return billingCycleDays;
    }

    public void setBillingCycleDays(int billingCycleDays) {
        this.billingCycleDays = billingCycleDays;
    }

    public int getNumberOfExistingUsers() {
        return numberOfExistingUsers;
    }

    public void setNumberOfExistingUsers(int numberOfExistingUsers) {
        this.numberOfExistingUsers = numberOfExistingUsers;
    }

    public int getMonthlyBill() {
        return monthlyBill;
    }

    public void setMonthlyBill(int monthlyBill) {
        this.monthlyBill = monthlyBill;
    }
}
