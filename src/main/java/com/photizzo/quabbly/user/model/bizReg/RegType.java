package com.photizzo.quabbly.user.model.bizReg;

import org.springframework.data.annotation.Id;

import java.util.List;

public class RegType {

    private List<String> aimOfBusiness;

    private String businessNature;

    public RegType() {
    }

    public RegType( List<String> aimOfBusiness, String businessNature) {

        this.aimOfBusiness = aimOfBusiness;
        this.businessNature = businessNature;
    }



    public List<String> getAimOfBusiness() {
        return aimOfBusiness;
    }

    public void setAimOfBusiness(List<String> aimOfBusiness) {
        this.aimOfBusiness = aimOfBusiness;
    }

    public String getBusinessNature() {
        return businessNature;
    }

    public void setBusinessNature(String businessNature) {
        this.businessNature = businessNature;
    }
}
