package com.photizzo.quabbly.user.repository;

import com.photizzo.quabbly.user.model.User;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<User, String> {

    User findByEmail(String email);

    Optional<User> findByUuidAndOrganisationId(String uuid, String tenantId);

    Optional<User> findByIdAndOrganisationId(String id, String tenantId);

    @Query(value = "{ 'organisationId' : ?0 }", fields = "{ 'firstname' : 1, 'lastname' : 1, 'email' : 1, 'uuid' : 1, 'createdDate' : 1}")
    List<User> findByOrganisationId(String id);

    List<User> findByEmailLike(String emailSuffix);

}