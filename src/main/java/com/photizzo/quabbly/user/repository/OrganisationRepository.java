package com.photizzo.quabbly.user.repository;

import com.photizzo.quabbly.user.model.Organisation;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface OrganisationRepository extends CrudRepository<Organisation, String> {

    Organisation findByName(String name);

    Optional<Organisation> findById(String id);

}