package com.photizzo.quabbly.user.repository.bizReg;

import com.photizzo.quabbly.user.model.bizReg.CompanyRegForm;
import org.springframework.data.repository.CrudRepository;

public interface CompanyRegRepository extends CrudRepository<CompanyRegForm, String> {

}
