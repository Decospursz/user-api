package com.photizzo.quabbly.user.repository.bizReg;

import com.photizzo.quabbly.user.model.bizReg.BusinessNameRegForm;
import org.springframework.data.repository.CrudRepository;

public interface BusinessNameRepository extends CrudRepository<BusinessNameRegForm, String> {

}
