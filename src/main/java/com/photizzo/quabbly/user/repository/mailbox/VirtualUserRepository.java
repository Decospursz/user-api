package com.photizzo.quabbly.user.repository.mailbox;

import com.photizzo.quabbly.user.model.mailbox.VirtualUser;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface VirtualUserRepository extends CrudRepository<VirtualUser, Long> {

    VirtualUser findByEmail(String email);
    List<VirtualUser> findAll();
}
