package com.photizzo.quabbly.user.repository;

import com.photizzo.quabbly.user.model.bizReg.CheckName;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CheckNameRepository extends CrudRepository<CheckName, String> {
    Optional<CheckName> findById(String id);
}
