package com.photizzo.quabbly.user.repository.mailbox;

import com.photizzo.quabbly.user.model.mailbox.VirtualAliases;
import org.springframework.data.repository.CrudRepository;

public interface VirtualAliasesRepository extends CrudRepository<VirtualAliases, Long> {

    public VirtualAliases findByDestination(String destination);

}
