package com.photizzo.quabbly.user.repository.mailbox;

import com.photizzo.quabbly.user.model.mailbox.VirtualDomain;
import org.springframework.data.repository.CrudRepository;

public interface VirtualDomainRepository extends CrudRepository<VirtualDomain, Long> {

    VirtualDomain findByName(String name);
}
