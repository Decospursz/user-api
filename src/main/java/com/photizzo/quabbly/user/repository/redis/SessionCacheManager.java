package com.photizzo.quabbly.user.repository.redis;

public interface SessionCacheManager {
    void storeTokenInCache(String key, String token);
    String getTokenFromCache(String key);
}
