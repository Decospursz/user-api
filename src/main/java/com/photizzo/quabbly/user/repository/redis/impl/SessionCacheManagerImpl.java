package com.photizzo.quabbly.user.repository.redis.impl;

import com.photizzo.quabbly.user.config.RedisUtil;
import com.photizzo.quabbly.user.repository.redis.SessionCacheManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class SessionCacheManagerImpl implements SessionCacheManager {

    private RedisUtil<String> redisUtilString;

    @Value("${session.cache.expiry}")
    private String sessionExpiry;

    @Autowired
    public SessionCacheManagerImpl(RedisUtil<String> redisUtilStudent) {
        this.redisUtilString = redisUtilStudent;
    }

    @Override
    public void storeTokenInCache(String key, String token){
        redisUtilString.putValueWithExpireTime(key, token, Long.parseLong(sessionExpiry), TimeUnit.MINUTES);
    }

    @Override
    public String getTokenFromCache(String key) {
        return redisUtilString.getValue(key);
    }
}
