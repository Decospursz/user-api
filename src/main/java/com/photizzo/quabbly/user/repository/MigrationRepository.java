package com.photizzo.quabbly.user.repository;

import com.photizzo.quabbly.user.model.migration.Migration;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MigrationRepository extends CrudRepository<Migration, String> {

    public List<Migration> findByStartedIsFalse();
}
